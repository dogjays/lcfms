<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="form-group">
    <label class="col-sm-2 control-label">
		${param.title}：
	</label>
	<div class="col-sm-2">
		<select name="s_province" class="form-control" url="${APP}" parent="0" default="${param.s_province}">
		<option value="0">省份</option>
		</select>
	</div>
	<c:if test="${param.level>1}">
	<div class="col-sm-2">
	 	<select name="s_city" class="form-control" url="${APP}" parent="${param.s_province}" default="${param.s_city}">
	 	<option value="0">地级市</option>
	 	</select>
	</div>
	</c:if>
	<c:if test="${param.level>2}">
	<div class="col-sm-2">
	 	<select name="s_county" class="form-control" url="${APP}" parent="${param.s_city}" default="${param.s_county}">
	 	<option value="0">区、县级市</option>
	 	</select>
	</div>	
	</c:if>	
	<c:if test="${param.level>3}">
	<div class="col-sm-2">
	 	<select name="s_street" class="form-control" url="${APP}" parent="${param.s_county}" default="${param.s_street}">
	 	<option value="0">街道</option>
	 	</select>  
	</div>	
	</c:if>			    
</div>
<script>

</script>