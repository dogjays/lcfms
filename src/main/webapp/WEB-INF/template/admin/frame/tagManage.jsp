<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
		<link rel="stylesheet" href="${CSS}font-awesome.css"/>
		<link rel="stylesheet" href="${CSS}widgets.css"/>
		<link rel="stylesheet" href="${CSS}b.page.bootstrap3.css"/>
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}widgets.js"></script>
		<script type="text/javascript" src="${JS}b.page.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="widget-box  widget-color-blue" id="3061">
					<div class="widget-header widget-header-small">
						<h6 class="widget-title">
							<i class="fa fa-sort">
							</i>
							标签管理
						</h6>
						<div class="widget-toolbar">
							<a href="javascript:void(0);" data-action="fullscreen">
								<i class="fa fa-expand">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="collapse">
								<i class="fa fa-chevron-up">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="close">
								<i class="fa fa-minus">
								</i>
							</a>
						</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
							<div class="widget-main"  style="padding: 12px;">
							    <c:forEach var="val" items="${list}">
							    <c:if test="${val.parentId eq 0}">
								<table class="bTable table table-striped table-bordered table-hover table-condensed">
								<thead>
									<tr>
										<th width="20%">标签名称(标签类型:${val.tagName})</th>
										<th width="15%">属性文件</th>
										<th width="15%">模版文件</th>
										<th width="15%">额外文件</th>
										<th width="10%">下载次数</th>
										<th width="15%">操作</th>
									</tr>
								</thead>
								<tbody>
								  <c:forEach var="mal" items="${list}">
								    <c:if test="${mal.parentId eq val.tagId}">
									<tr>
										<td>${mal.tagName}</td>
										<td>${mal.tagFile}</td>
										<td>${mal.tempFile}</td>
										<td>${mal.extraFile}</td>
										<td>${mal.download}</td>
										<td>
										<a href="addview?tagId=${mal.tagId}"><i class="fa fa-edit bigger-120"></i>修改 </a>&nbsp;&nbsp;
										<a href="delete?tagId=${mal.tagId}"><i class="fa fa-trash bigger-120"></i>删除 </a>
										</td>
									</tr>
									</c:if>
								  </c:forEach>		
								</tbody>
							</table>
							</c:if>
							</c:forEach>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>					
		$(function(){
			widget.init();			
		});
		</script>
	</body>
</html>
