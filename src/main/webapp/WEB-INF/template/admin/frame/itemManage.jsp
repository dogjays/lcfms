<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>欢迎使用老成FMS (5.0正式版)-java web框架</title>
		<meta name="keywords" content="java快速开发框架,java编程,java web"> 
	    <meta name="description" content="老成fms(Frame Management System)，是一款前端，后台，业务集成的快速开发框架">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="${APP}statics/ace/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${APP}statics/ace/css/font-awesome.min.css"/>	
		<link rel="stylesheet" href="${APP}statics/ace/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="${APP}statics/ace/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="${APP}statics/ace/css/ace-skins.min.css" />
		<link rel="stylesheet" href="${APP}statics/ace/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="${APP}statics/ace/css/ace-ie.min.css" />
		<![endif]-->		
		<!--[if !IE]> -->
		<script src="${APP}statics/ace/js/jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->	
		<!--[if IE]>
		<script src="${APP}statics/ace/js/jquery-1.11.3.min.js"></script>
		<![endif]-->	
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='${APP}statics/ace/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>	
		<script src="${APP}statics/ace/js/bootstrap.min.js"></script>
		<script src="${APP}statics/ace/js/ace-elements.min.js"></script>
		<script src="${APP}statics/ace/js/ace.min.js"></script>
		<script src="${APP}statics/ace/js/ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="${APP}statics/ace/js/html5shiv.min.js"></script>
		<script src="${APP}statics/ace/js/respond.min.js"></script>
		<![endif]-->
		<script src="${APP}statics/ace/js/jquery.nestable.min.js"></script>
		<style>
		.dd{max-width: 100%;}
		</style>
	</head>
	<body>
	<div class="main-container ace-save-state" id="main-container">
	<div class="main-content">
	<div class="main-content-inner">		
	<script type="text/javascript">
		jQuery(function($){		
			$.get("getTree?parentId=0",function(res){
				$("#nestable").html(res);
				$('.dd').nestable();				
				$('[data-rel="tooltip"]').tooltip();		
			});
		});
		
		function saveSort(){
			$('#modal-table').modal('toggle');
			var dditem=$("#nestable .dd-item");
			var i = 0;
			    (function(){
			        if(i>=dditem.length){location.href='manage';return false;}
			        var itemId=$(dditem[i]).attr("data-id");
					var parentId=$(dditem[i]).parent().attr("parentId");
					if(parentId==null){
						parentId=$(dditem[i]).parent().parent().attr("data-id");
					}
					$.post("sort",{
			            	sort:i,
			            	itemId:itemId,
			            	parentId:parentId	            	
			        },function (res) {
			            	var str="<p style=\"margin-bottom:0px\">"+res+"更新完毕。<i class=\"fa fa-check green bigger-130\"></i></p>";
			                $("#modalConetent").prepend(str);
			        });
					i++;
			        window.setTimeout(arguments.callee,100);
			 })();			
		}
		
		function deleteItem(id){
			$.get("delete?itemId="+id,function(){
				$("li[data-id='"+id+"']").remove();
			});
		}
	</script>
	<div class="widget-box widget-color-orange">
		<div class="widget-header widget-header-small">
			<h6 class="widget-title">
				<i class="ace-icon fa fa-sort"></i>
				栏目管理
			</h6>
	
			<div class="widget-toolbar">
				<div class="widget-menu">
					<a href="#" data-action="settings" data-toggle="dropdown" aria-expanded="false">
						<i class="ace-icon fa fa-bars"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer" style="width: 50px;">
						<li>
							<a  href="additem"><i class="ace-icon fa fa-plus"></i>&nbsp;&nbsp;添加栏目</a>
						</li>
						<li>
							<a href="javascript:saveSort();"><i class="ace-icon fa fa-save"></i>&nbsp;&nbsp;保存排序</a>
						</li>
					</ul>
				</div> 
				<a href="#" data-action="collapse">
					<i class="ace-icon fa fa-minus" data-icon-show="fa-plus" data-icon-hide="fa-minus"></i>
				</a> 
				<a href="#" data-action="close">
					<i class="ace-icon fa fa-times"></i>
				</a> 
			</div>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<table class="table table-bordered table-hover table-condensed" style="border: 0;">
					<thead>
						<tr>
							<th width="140px">栏目Id</th>
							<th>栏目名称</th>
							<th width="250px">URL</th>
							<th width="140px">管理操作</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4" style="background: #fff;border: 0;padding: 0;">						    
								<div class="dd" id="nestable"></div>	
							</td>
						</tr>
					</tbody>
				</table>	
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-table">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					 <h3 id="modalTitle"><i class="icon-spinner icon-spin orange bigger-125"></i>更新栏目中，请稍后。。</h3>
				</div>
				<div class="modal-body" id="modalConetent" style="overflow-y:hidden;max-height:350px;"></div>
			</div>
		</div>
	</div>
	</div>
	</div><!-- /.main-content -->
	</div><!-- /.main-container -->	
	</body>
</html>