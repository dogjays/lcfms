<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="form-group">
    <label class="col-sm-2 control-label">${pTitle}栏目：</label>	
    <div id="showSelectBox"></div>
	<div class="col-sm-2">
		<a href="javascript:getCateData();" style="position:relative;top:6px;">查看子栏目</a>
	</div>		    
</div>
<script>
    function getCateData(){
    	var l=$("select[name='category_ids']").length;
    	var id=0;
    	if(l>0){
    		id=$("select[name='category_ids']")[l-1].value;
    	}
    	$.get("${APP}admin/category/getCategory?id="+id,function(res){
    		eval("var json="+res);  
    		if(json.length==0){
    			$.alert({
    				title:'提示',
    				content:"已经没有子栏目了",
    				style:'alert-danger',
    				position:'topRight',
    				closeBtn:true,
    				autoClose:true
    			});
    			return;
    		}
    		str="<div class=\"col-sm-2\">";
    		str+="<span class=\"label-default badge\" onclick=\"clearNext(this,1);\" style=\"position:absolute;right: 5px;top:-8px;cursor:pointer;\"><i class=\"fa fa-times\"></i></span>";
    		str+="<select name=\"category_ids\" class=\"form-control\" onchange=\"clearNext(this);\">";
    		for(var i=0;i<json.length;i++){   			
    			str+="<option value=\""+json[i].id+"\">"+json[i].title+"</option>"   			  			
    		}  	
    		str+="</select>";	
			str+="</div>";	 
    		$("#showSelectBox").append(str);
    	});      	
    }  
    
    function clearNext(element,type){    	
    	var m=$($(element).parent()).nextAll();
    	m.remove();
		if(type==1){
			$(element).parent().remove();
    	}
    }    
    
    <c:if test="${USERINFO.mca[1] eq 'Article'}">
    getCateData()
    </c:if>
</script>
