<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>																			
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css">
		<link rel="stylesheet" href="${CSS}font-awesome.css">
		<link rel="stylesheet" href="${CSS}form.css">
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}form.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="form-box form-color-grey">
					<div class="form-header">
						<h6 class="form-title">
							<i class="fa fa-sort">
							</i>
							<lc:if test="${group.gid ne null && group.gid ne 0}">修改用户组</lc:if><lc:else>添加用户组</lc:else>
						</h6>
					</div>
					<form class="form-horizontal" method="post" action="add_group" name="myform">
			    		<input type="hidden" name="gid" value="${group.gid}"/>
						<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户组名称：
								</label>
								<div class="col-sm-10">
									<input class="form-control" value="${group.gname}" name="gname" type="text" attribute="unempty unNum"
									placeholder="请输入用户组名称" error="输入有误" success="输入正确">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户组描述：
								</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="gdesc" rows="5" cols="50" maxnum="50">${group.gdesc}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									控制器权限：
								</label>
								<div class="col-sm-10">
									<div class="checkbox">
									    <c:forEach var="tle" varStatus="key" items="${permission_type_action}">
									    <label class="checkbox-inline" style="position:relative;left:-20px;top:5px;">${key.count}、${tle}</label>
										<div class="checkbox">
										<c:forEach var="aal" items="${permission_action}">
										<c:if test="${aal.type eq tle}">
										<label>
											<input name="gpermission" type="checkbox" value="${aal.id}"
											<c:forEach var="sal" items="${GroupPermission}">
											<c:if test="${sal eq aal.id}">checked</c:if>
											</c:forEach>
											/>
											<span class="lbl"> ${aal.name}</span>
										</label>
										</c:if>							
										</c:forEach>																							
										</div>	
										</c:forEach>				
									</div>									
								</div>
							</div>
						</div>
						<div class="form-footer">
							<div class="form-group">
								<label class="col-sm-2">
								</label>
								<div class="col-sm-10">
									<button type="button" id="save" class="btn btn-sm btn-grey">
										<i class="fa fa-check">
										</i>
										提交
									</button>
									<button type="reset" id="reset" class="btn btn-sm btn-grey">
										<i class="fa fa-undo">
										</i>
										重置
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>			
		var myform=form.init("myform");	
		$("#save").click(function(){
			//可以自定义提示正确或错误信息
			//myform.error("表单的name","提示的信息");
			//myform.success("表单的name","提示的信息");
			//可以focus到具体表单
			//myform.focus("表单的name");
			myform.submit();
		});
		$("#reset").click(function(){
			myform.reset();
		});
		</script>
	</body>
</html>
				