<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>																	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css">
		<link rel="stylesheet" href="${CSS}font-awesome.css">
		<link rel="stylesheet" href="${CSS}form.css">
		<link rel="stylesheet" href="${CSS}kalendae.css"/>
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}../../layer/layer.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}kalendae.js"></script>	
		<script src="${JS}form.js"></script>
		<script src="${JS}alert.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="form-box form-color-grey">
					<div class="form-header">
						<h6 class="form-title">
							<i class="fa fa-sort">
							</i>
							${title}
						</h6>
					</div>
					<form class="form-horizontal" name="myform" method="${method}" action="${action}">		            	
						<div class="form-body">
						    <c:forEach var="iy" begin="0" end="${form.size()}">						  
							<c:forEach items="${form}" var="val">						
							<c:if test="${val.sort == iy}">				
							<%-- hidden类型   --%>						
							<c:if test="${val.type eq 'hidden'}">
							<input type="hidden" name="${val.name}" value="${val.value}"/>
							</c:if>
							<%-- jspview类型 --%>
							<c:if test="${val.type eq 'jspview'}">
							<jsp:include page="../${val.file}"/>
							</c:if>
							<%-- input类型 --%>
							<c:if test="${val.type eq 'input'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
									<input class="form-control" name="${val.name}" type="text" value="${val.value}" attribute="${val.validate}"
									<lc:if test="${val.placeholder != null}">placeholder="${val.placeholder}"</lc:if>								
									<lc:else>placeholder="请输入${val.title}"</lc:else>	
									<lc:if test="${val.readonly != null}">readonly</lc:if>
									/>						
								</div>
							</div>
							</c:if>
							<%-- editor类型 --%>
							<c:if test="${val.type eq 'editor'}">
							<jsp:include page="../kindeditor.jsp">
								<jsp:param name="title" value="${val.title}"/>
								<jsp:param name="name" value="${val.name}"/>
								<jsp:param name="value" value="${val.value}"/>
							</jsp:include>
							</c:if>
							<%-- uploadImg类型 --%>
							<c:if test="${val.type eq 'uploadImg'}">
							<jsp:include page="../imageupload.jsp">
								<jsp:param name="title" value="${val.title}"/>
							</jsp:include>
							</c:if>
							<%-- uploadFile类型 --%>
							<c:if test="${val.type eq 'uploadFile'}">
							<jsp:include page="../fileupload.jsp">
								<jsp:param name="title" value="${val.title}"/>
								<jsp:param name="fileType" value="${val.fileType}"/>
								<jsp:param name="fileNum" value="${val.fileNum}"/>
								<jsp:param name="fileSize" value="${val.fileSize}"/>
							</jsp:include>
							</c:if>
							<%-- textarea类型 --%>
							<c:if test="${val.type eq 'textarea'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="${val.name}" rows="3" cols="50" maxnum="${val.maxnum}">${val.value}</textarea>					
								</div>
							</div>
							</c:if>
							<%-- areas类型 --%>
							<c:if test="${val.type eq 'areas'}">
							<jsp:include page="../areas.jsp">
								<jsp:param name="title" value="${val.title}"/>
								<jsp:param name="level" value="${val.level}"/>
								<jsp:param name="s_province" value="${val.defaultValue[0]}"/>
								<jsp:param name="s_city" value="${val.defaultValue[1]}"/>
								<jsp:param name="s_county" value="${val.defaultValue[2]}"/>
								<jsp:param name="s_street" value="${val.defaultValue[3]}"/>
							</jsp:include>
							</c:if>
							<%-- dateslice类型 --%>
							<c:if test="${val.type eq 'dateslice'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
								    <div class="row">
								    	<div class="col-sm-4"><input readonly="readonly" class="form-control calendar" value="${val.begin}" name="calendar" type="text" months="1" mode="single" direction="${direction}"></div>
								    	<div class="col-sm-1" style="text-align:center;padding:0;"><span style="position:relative;top:5px;">到</span></div>
								    	<div class="col-sm-4"><input readonly="readonly" class="form-control calendar" value="${val.end}" name="calendar" type="text" months="1" mode="single" direction="${direction}"></div>
								    </div>						
								</div>
							</div>							
							</c:if>
							<%-- timeslice类型 --%>
							<c:if test="${val.type eq 'timeslice'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10"></div>
							</div>							
							</c:if>
							<%-- date类型 --%>
							<c:if test="${val.type eq 'date'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
								<input readonly="readonly" class="form-control calendar" value="${val.value}" name="calendar" type="text" months="1" mode="single" direction="${direction}">      					
								</div>
							</div>							
							</c:if>
							<%-- select类型 --%>
							<c:if test="${val.type eq 'select'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
									<select class="form-control" name="${val.name}">
										<c:set var="i" value="0"/>	
										<c:forEach items="${val.value}" var="option">																		
										<option value="${option}" <c:if test="${val.def eq option}">selected</c:if>>
										<c:out value="${val.text[i]}"/>								
										</option>
										<c:set var="i" value="${i+1}"/>	
										</c:forEach>										
									</select>
								</div>
							</div>
							</c:if>	
							<%-- radio类型 --%>
							<c:if test="${val.type eq 'radio'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
									<div class="form-control radio">
										<c:set var="i" value="0"/>	
										<c:forEach items="${val.value}" var="_radio">																		
										<label class="radio-inline">
											<input name="${val.name}" type="radio" value="${_radio}" <c:if test="${val.def eq _radio}">checked="checked"</c:if>>
											<span class="lbl">
												<c:out value="${val.text[i]}"/>	
											</span>
										</label>						
										<c:set var="i" value="${i+1}"/>	
										</c:forEach>													
									</div>
								</div>
							</div>
							</c:if>	
							<%-- checkbox类型 --%>
							<c:if test="${val.type eq 'checkbox'}">
							<div class="form-group">
								<label class="col-sm-2 control-label">${val.title}：</label>
								<div class="col-sm-10">
									<div class="form-control checkbox">
										<c:set var="i" value="0"/>	
										<c:forEach items="${val.value}" var="_checkbox">																		
										<label class="checkbox-inline">
											<input name="${val.name}" type="checkbox" value="${_checkbox}" <c:if test="${val.def.contains(_checkbox)}">checked="checked"</c:if>>
											<span class="lbl">
												<c:out value="${val.text[i]}"/>	
											</span>
										</label>						
										<c:set var="i" value="${i+1}"/>	
										</c:forEach>													
									</div>
								</div>
							</div>
							</c:if>	
							</c:if>					
							</c:forEach>
							</c:forEach>
						</div>
						<div class="form-footer">
							<div class="form-group">
								<label class="col-sm-2">
								</label>
								<div class="col-sm-10">
									<button type="button" id="save" class="btn btn-sm btn-grey">
										<i class="fa fa-check">
										</i>
										提交
									</button>
									<button type="reset" id="reset" class="btn btn-sm btn-grey">
										<i class="fa fa-undo">
										</i>
										重置
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>	
		var myform=form.init("myform");			
		$("#save").click(function(){
			myform.submit();
		});	
		$("#reset").click(function(){
			myform.reset();
		});
		
		function openNewTab(itemId,url,itemName){
			if(url.indexOf("http")==-1){
				var c="${USERINFO.mca[1]}".toLowerCase();
				url="${APP}${USERINFO.mca[0]}/"+c+"/"+url;
			}
			parent.openNewTab(itemId,url,itemName)	
		}
		
		function openlayer(title,icon,url,width,height){
			if(title=='删除' && icon=='fa-trash'){
				layer.confirm('您确定要删除吗？',{btn: ['是的','取消']}, function(){
					 location.href=url;
				});
			}else{
				layer.open({
				      type: 2,
				      title: '<i class="fa '+icon+'"></i> '+title,
				      maxmin: true,
				      shadeClose: true, //点击遮罩关闭层
				      area : [width+'px' , height+'px'],
				      content: url
				});
			}	
		}
		</script>
	</body>
</html>									