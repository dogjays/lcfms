<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
		<link rel="stylesheet" href="${CSS}font-awesome.css"/>
		<link rel="stylesheet" href="${CSS}widgets.css"/>
		<link rel="stylesheet" href="${CSS}b.page.bootstrap3.css"/>
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}../../layer/layer.js"></script>
		<script src="${JS}widgets.js"></script>
		<script type="text/javascript" src="${JS}b.page.js"></script>
		<style type="text/css">
		.sc-box{margin:10px auto;border:1px solid #c2c2c2;box-shadow: 0px 0px 10px -2px;max-width:300px;}
		.sc-box-top{height:140px;position: relative;padding:12px 15px;}
		.sc-box-top>.ylwz{display:none;line-height:140px;position: absolute;top:0;left:0;text-align: center;font-weight: bold;color: #fff;cursor:pointer;width:100%;height:100%;background:rgba(130, 130, 130, 0.72);}	
		.sc-box-top>p{position: absolute;bottom: 15px;width: 90%;background: rgba(39, 35, 35, 0.56);margin: 0;text-align: center;line-height: 28px;font-weight: bold;color: #fff;}
		.sc-box-next{position: relative;height:77px;padding:8px 15px;border-top:1px solid rgba(183, 183, 183, 0.35);font-size:12px;}
		.sc-box-next>.ct{width:60%;float:left;}
		.sc-box-next i{display: block;width: 60px;height: 60px;float:right;}
		.sc-box-next>.ylwz{display:none;line-height:60px;position: absolute;top:0;left:0;text-align: center;font-weight: bold;color: #fff;cursor:pointer;width:100%;height:100%;background:rgba(130, 130, 130, 0.72);}	
		.sc-box-bottom{font-size:12px;padding:12px 15px;color:grey;position:relative;}
		.sc-box-bottom>.ed{position:absolute;right:0;top:10px;padding-right:25px}
		.sc-box-bottom>.ed>span{margin-right:10px;font-size:15px;cursor:pointer;}
		.leftbutton{margin-top:20px;}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="widget-box  widget-color-blue" id="3061">
					<div class="widget-header widget-header-small">
						<h6 class="widget-title">
							<i class="fa fa-sort">
							</i>
							素材管理
						</h6>
						<div class="widget-toolbar">
							<a href="javascript:void(0);" data-action="fullscreen">
								<i class="fa fa-expand">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="collapse">
								<i class="fa fa-chevron-up">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="close">
								<i class="fa fa-minus">
								</i>
							</a>
						</div>
					</div>					
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
						<div class="widget-main" style="padding: 12px;"> 
						   <div class="container-fluid">
						   <div class="row">
						   		<div class="col-sm-2">
						   		<div class="leftbutton">
									<button type="button" class="btn <c:if test="${type eq 'news'}">btn-success</c:if><c:if test="${type ne 'news'}">btn-grey</c:if>" id="news">
										<i class="fa fa-book align-top bigger-125">
										</i>
										图文
									</button>
								</div>
								<div class="leftbutton">
									<button type="button" class="btn <c:if test="${type eq 'image'}">btn-success</c:if><c:if test="${type ne 'image'}">btn-grey</c:if>" id="image">
										<i class="fa fa-image align-top bigger-125">
										</i>
										图片
									</button>
								</div>
								<div class="leftbutton">
									<button type="button" class="btn <c:if test="${type eq 'video'}">btn-success</c:if><c:if test="${type ne 'video'}">btn-grey</c:if>" id="video">
										<i class="fa fa-video-camera align-top bigger-125">
										</i>
										视频
									</button>
								</div>
								<div class="leftbutton">
									<button type="button" class="btn <c:if test="${type eq 'voice'}">btn-success</c:if><c:if test="${type ne 'voice'}">btn-grey</c:if>" id="voice">
										<i class="fa fa-headphones align-top bigger-200">
										</i>
										语音
									</button>
								</div>								
						   		</div>
						   		<div class="col-sm-10">
							   		<c:forEach items="${list}" var="v">
									<div class="col-sm-4">
									    <div class="sc-box">
									        <c:if test="${type eq 'news'}">
									        <c:set var="im" value="0"/>	
									        <c:forEach items="${v.content}" var="s" >
									        <c:if test="${im==0}">
									        <div class="sc-box-top" style="background:url(${s.thumb_url});background-origin: content-box;background-repeat: no-repeat;background-clip: content-box;background-size: 100% 100%;" url="${s.url}">
												<p>${s.title}</p>
												<div class="ylwz">预览文章</div>
											</div>
									        </c:if>
									        <c:if test="${im!=0}">
									        <div class="sc-box-next" url="${s.url}">
												<div class="ct">${s.title}</div>
												<i style="background:url(${s.thumb_url}) 100% no-repeat;"></i>
												<div class="ylwz">预览文章</div>
											</div>
									        </c:if>
									        <c:set var="im" value="${im+1}"/>	
									        </c:forEach>	
											<div class="sc-box-bottom">
												<div class="ct">更新于 ${v.update_time}</div>
												<div class="ed">
													<span class="showMediaid" data-toggle="tooltip" data-placement="top" title="" data-original-title="查看media_id" media_id="${v.media_id}"><i class="fa fa-comment"></i></span>
													<span class="deleteMaterial" data-toggle="tooltip" data-placement="top" title="" data-original-title="删除" media_id="${v.media_id}"><i class="fa fa-trash"></i></span>
												</div>
											</div>
									        </c:if>
									    	<c:if test="${type eq 'image'}">
									    	<div class="sc-box-bottom">
									    	 	<div class="sc-box-top" style="background:url(${v.url});background-origin: content-box;background-repeat: no-repeat;background-clip: content-box;background-size: 100% 100%;" url="${v.url}">
													<p>${s.name}</p>
													<div class="ylwz">预览图片</div>
											    </div>
												<div class="ct">更新于 ${v.update_time}</div>
												<div class="ed">
													<span class="showMediaid" data-toggle="tooltip" data-placement="top" title="" data-original-title="查看media_id" media_id="${v.media_id}"><i class="fa fa-comment"></i></span>
													<span class="deleteMaterial" data-toggle="tooltip" data-placement="top" title="" data-original-title="删除" media_id="${v.media_id}"><i class="fa fa-trash"></i></span>
												</div>
											</div>
									    	</c:if>
										</div>
									</div>	
									</c:forEach>
						   		</div>
							</div>
						    </div>
						</div>
						</div>
					</div>
					<div id="page" pagenumber="1" pagesize="6" totalrow="${count}"></div>	
				</div>
			</div>
		</div>
		<script>					
		$(function(){
			widget.init();
			//当前页	
			var pageNumber=$('#page').attr("pageNumber");
			//每页多少行	
			var pageSize=$('#page').attr("pageSize");
			//总共多少行
			var totalRow=$('#page').attr("totalRow");
			//总共多少页
			var totalPage=Math.ceil(totalRow/pageSize);
			$('#page').bPage({  
			    url : '',
			    totalPage :totalPage,
			    totalRow :totalRow,
			    pageSize :pageSize,
			    pageNumber:pageNumber
			});	
			
			$('[data-toggle=tooltip]').tooltip();
			
			$(".sc-box-top").click(function(){
				var url=$(this).attr("url");
				window.open(url);
			});
			
			$(".sc-box-next").click(function(){
				var url=$(this).attr("url");
				window.open(url);
			});
			
			$(".sc-box-top,.sc-box-next").mouseover(function(){
				$(this).children(".ylwz").show();
			});
			
			$(".sc-box-top,.sc-box-next").mouseout(function(){
				$(this).children(".ylwz").hide();
			});
			
			$(".showMediaid").click(function(){
				var id=$(this).attr("media_id");
				layer.alert(id);
			});
			
			$(".deleteMaterial").click(function(){
				var id=$(this).attr("media_id");
				layer.confirm('您确定要删除该素材吗？',{btn: ['是的','取消']}, function(){
					$.get("deleteMaterial?media_id="+id,function(res){
						layer.msg(res.msg);	
						if(res.code==1){
							location.href=res.url;
						}							
					});
				});	
			});
			
			$(".leftbutton>button").click(function(){
				var id=$(this).attr("id");
				location.href="list?type="+id;
			});
		
		});
		
		</script>
	</body>
</html>