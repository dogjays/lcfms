<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>																
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="${CSS}bootstrap.min.css">
	<link rel="stylesheet" href="${CSS}font-awesome.css">
	<link rel="stylesheet" href="${CSS}widgets.css">	
	<link rel="stylesheet" href="${CSS}tabs.css">
	<link rel="stylesheet" href="${CSS}form.css">
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}../../layer/layer.js"></script>
	<script src="${JS}widgets.js"></script>
	<script src="${JS}form.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="widget-box  widget-color-blue" id="6264">
			<div class="widget-header widget-header-small">
				<h6 class="widget-title">
					<i class="fa fa-sort">
					</i>
					自定义菜单
				</h6>
				<div class="widget-toolbar">
					<a href="javascript:void(0);" data-action="fullscreen">
						<i class="fa fa-expand">
						</i>
					</a>
					<a href="javascript:void(0);" data-action="collapse">
						<i class="fa fa-chevron-up">
						</i>
					</a>
					<a href="javascript:void(0);" data-action="close">
						<i class="fa fa-minus">
						</i>
					</a>
				</div>
				<div class="widget-toolbar no-border">				
					<button class="btn btn-xs btn-default smaller" id="upload">
						<i class="fa fa-arrow-up"></i> 同步到微信
					</button>
					<button class="btn btn-xs btn-default smaller" id="clearMenu">
						<i class="fa fa-times"></i> 清空微信菜单
					</button>
					<button class="btn btn-xs btn-default smaller" id="addChild">
						<i class="fa fa-plus"></i> 添加子菜单
					</button>
				</div>
			</div>
			<div class="widget-body">
				<div class="widget-body-inner" style="display: block;">
					<div class="widget-main" style="padding: 12px;">
						<div class="tabbable">
							<ul class="nav nav-tabs">
							    <c:forEach items="${list}" var="lm">
							    <c:if test="${lm.pid==0}">
							    <lc:if test="${lm.type eq 'plus'}">
								<li class="${lm.active}">
									<a data-toggle="tab" href="#tab_${lm.id}">
										${lm.title}												
										<i class="fa fa-plus"></i>																			
									</a>
								</li>	
								</lc:if>
								<lc:elseif test="${lm.type eq 'sub_button'}">
								<li class="${lm.active}">
									  <a class="btn" data-toggle="tab" href="#tab_${lm.id}">${lm.title}</a>
								</li>
								<li>
									  <a class="btn" data-toggle="dropdown">
									    <i class="fa fa-caret-down"></i>
									  </a>
									  <ul class="dropdown-menu">
									    <c:set var="im" value="0"/>	
					 				    <c:forEach items="${list}" var="fm">
					 				    <c:if test="${lm.id==fm.pid}">
									    <li><a class="dropdown-item" href="${APP}weixin/menu/showMenu?id=${fm.id}">${fm.title}</a></li>
									    <c:set var="im" value="${im+1}"/>	
									    </c:if>
									    </c:forEach>	
									    <c:if test="${im==0}">
									    <li><a class="dropdown-item" href="#">无</a></li>
									    </c:if>							    	
									  </ul>
								</li>
								</lc:elseif>
								<lc:else>
								<li class="${lm.active}">
									<a data-toggle="tab" href="#tab_${lm.id}">
										${lm.title}												
										<i class="fa fa-pencil"></i>																			
									</a>
								</li>	
								</lc:else>	
								</c:if>					
								</c:forEach>	
							</ul>
							<div class="tab-content">
								<div class="form-box">
									<form class="form-horizontal" name="myform" method="AJAX" action="saveMenu">
									    <input type="hidden" name="id" value="${menu.id}"/>
										<div class="form-body">
										    <c:if test="${menu.pid!=0}">
										    <div class="form-group">
												<label class="col-sm-2 control-label">
													上级菜单：
												</label>
												<div class="col-sm-10">
													<select class="form-control" name="pid">
													<c:forEach items="${list}" var="lm">
							    					<c:if test="${lm.pid==0 && lm.id!=0 && lm.type eq 'sub_button'}">
													<option value="${lm.id}" <c:if test="${lm.id eq menu.pid}">selected</c:if>>${lm.title}</option>
													</c:if>
													</c:forEach>
													</select>
												</div>
											</div>
											</c:if>
											<div class="form-group">
												<label class="col-sm-2 control-label">
													菜单标题：
												</label>
												<div class="col-sm-10">
													<input class="form-control" name="title" type="text" value="${menu.title}" attribute="unempty" placeholder="输入内容">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">
													菜单类型：
												</label>
												<div class="col-sm-10">
													<div class="radio">
													    <c:if test="${menu.pid==0}">
														<label class="radio-inline">
															<input name="type" type="radio" value="sub_button" <c:if test="${menu.type eq 'sub_button'}">checked</c:if>>
															<span class="lbl">菜单</span>
														</label>
														</c:if>
														<label class="radio-inline">
															<input name="type" type="radio" value="textMessage" <c:if test="${menu.type eq 'textMessage'}">checked</c:if>>
															<span class="lbl">文本消息</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="media_id" <c:if test="${menu.type eq 'media_id'}">checked</c:if>>
															<span class="lbl">下发图文消息</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="view_limited" <c:if test="${menu.type eq 'view_limited'}">checked</c:if>>
															<span class="lbl">跳转图文消息</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="view" <c:if test="${menu.type eq 'view'}">checked</c:if>>
															<span class="lbl">跳转链接</span>
														</label>
													</div>
													<div class="radio">												    
														<label class="radio-inline">
															<input name="type" type="radio" value="scancode_push" <c:if test="${menu.type eq 'scancode_push'}">checked</c:if>>
															<span class="lbl">微信扫一扫</span>
														</label>
														<span data-toggle="tooltip" data-placement="top" title="" data-original-title="直接将扫描的结果发送给用户，如果是URL则跳转到URL，并将结果发送给开发者"><i class="fa fa-exclamation-circle"></i></span>
														<label class="radio-inline">
															<input name="type" type="radio" value="scancode_waitmsg" <c:if test="${menu.type eq 'scancode_waitmsg'}">checked</c:if>>
															<span class="lbl">微信扫一扫</span>
														</label>
														<span data-toggle="tooltip" data-placement="top" title="" data-original-title="用户扫描结束后会关闭相机，并将结果发送给开发者"><i class="fa fa-exclamation-circle"></i></span>
														<label class="radio-inline">
															<input name="type" type="radio" value="pic_sysphoto" <c:if test="${menu.type eq 'pic_sysphoto'}">checked</c:if>>
															<span class="lbl">调用照相机</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="pic_photo_or_album" <c:if test="${menu.type eq 'pic_photo_or_album'}">checked</c:if>>
															<span class="lbl">调用系统相册</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="pic_weixin" <c:if test="${menu.type eq 'pic_weixin'}">checked</c:if>>
															<span class="lbl">调用微信相册</span>
														</label>
														<label class="radio-inline">
															<input name="type" type="radio" value="location_select" <c:if test="${menu.type eq 'location_select'}">checked</c:if>>
															<span class="lbl">获取位置</span>
														</label>
													</div>
												</div>
											</div>
											<div class="form-group sv_view hideform" style="display:none;">
												<label class="col-sm-2 control-label">
													跳转地址：
												</label>
												<div class="col-sm-10">
													<input class="form-control" name="url" type="text" value="${menu.url}" placeholder="输入内容">
												</div>
											</div>
											<div class="form-group sv_view_limited sv_media_id hideform" style="display:none;">
												<label class="col-sm-2 control-label">
													media_id：
												</label>
												<div class="col-sm-10">
													<input class="form-control" name="media_id" type="text" value="${menu.media_id}" placeholder="可以到素材管理中去查看media_id">
												</div>
											</div>
											<div class="form-group sv_textMessage hideform" style="display:none;">
												<label class="col-sm-2 control-label">
													发送内容：
												</label>
												<div class="col-sm-10">
													<textarea class="form-control" name="message" rows="5" cols="50" maxnum="500">${menu.message}</textarea>	
												</div>
											</div>
											<div class="form-group sv_sub_button hideform" style="display:none;">
												<label class="col-sm-2 control-label">
													配置子菜单：
												</label>
												<div class="col-sm-10">
													<div class="checkbox">
														<c:forEach items="${list}" var="lm">
								    					<c:if test="${lm.pid!=0}">
														<label class="checkbox-inline">
															<input name="child" type="checkbox" value="${lm.id}" <c:if test="${lm.pid eq menu.id}">checked disabled</c:if>>
															<span class="lbl">${lm.title}</span>
														</label>	
														</c:if>
														</c:forEach>												
													</div>
												</div>
											</div>
										</div>
										<div class="form-footer">
											<div class="form-group">
												<label class="col-sm-2">
												</label>
												<div class="col-sm-10">
													<button type="button" id="save" class="btn btn-sm btn-grey">
														<i class="fa fa-check">
														</i>
														保存数据
													</button>
													<button type="reset" id="reset" class="btn btn-sm btn-grey">
														<i class="fa fa-trash">
														</i>
														删除菜单
													</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
widget.init();
$('[data-toggle=tooltip]').tooltip();
var myform=form.init("myform");
$("#save").click(function(){
	var type=myform.getValue("type");
	var url=myform.getValue("url");
	if(type=='view' && url.indexOf("http://")!=0){
		myform.error("url","url必须为以（http://）或（https://）开头的网址");
		return;
	}
	myform.submit();
});

$("#reset").click(function(){
	$.get("deleteMenu?id=${menu.id}",function(res){
		layer.msg(res.msg);
		if(res.code==1){
			setTimeout(function(){
        		location.href=res.url;
    		},1000);
		}
	});
});

$("#addChild").click(function(){
	if('sub_button'=='${menu.type}'){
		location.href='showMenu?pid=${menu.id}';
	}else{
		layer.msg("你的上级菜单不是菜单类型，不能添加子菜单！");
	}
});

$("#upload").click(function(){
	layer.confirm('您确定要同步所有的微信菜单吗？',{btn: ['是的','取消']}, function(){
		$.get("upload",function(res){
			layer.msg(res.msg);		
		});
	});		
});

$("#clearMenu").click(function(){
	layer.confirm('您确定要清空所有的微信菜单吗？',{btn: ['是的','取消']}, function(){
		$.get("clearMenu",function(res){
			layer.msg(res.msg);		
		});
	});	
});

$("input[name='type']").change(function(){
	var v=$(this).val();
	showSV(v)	
});

$("a[data-toggle='tab']").click(function(){
	var h=$(this).attr("href");
	var id=h.substr(5);
	location.href="showMenu?id="+id;
});

function showSV(v){	
	$(".hideform").hide();
	$(".sv_"+v).show();
}

var type="${menu.type}";
showSV(type);
</script>
</body>
</html>				
				
				
				