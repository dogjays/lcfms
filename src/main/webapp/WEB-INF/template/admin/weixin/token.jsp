<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<div class="form-group">
	<label class="col-sm-2 control-label">令牌(Token)：</label>
	<div class="col-sm-8">
		<input class="form-control" name="token" type="text" value="${token}"/>						
	</div>
	<div class="col-sm-2">
	<button type="button" class="btn btn-grey" id="changeToken">
			<i class="fa fa-check align-top bigger-125">
			</i>
			生成token
	</button>
	</div>
</div>
<script>
	$("#changeToken").click(function(){
		var w="qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
		var r="";
		for(var i=0;i<32;i++){
			var j=Math.floor(Math.random()*(w.length-1));
			r+=w[j];
		}
		document.myform.token.value=r;
	});
	$(function(){
		myform.success("access_token","可以从你授权的服务器上拷贝最新的access_token，粘贴保存，用于本地调试，支持junit");
	});
</script>