<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="${CSS}dropzone.css"/>
<script type="text/javascript" src="${JS}dropzone.min.js"></script>
<div class="form-group">
	<label class="col-sm-2 control-label">${param.title}：</label>
    <div class="col-sm-10">
      	<div class="dropzone dz-clickable" maxFilesize="2" url="${APP}admin/common/fileupload" acceptedFiles="image/*" maxFiles="10">
			<div class="dz-default dz-message">			
				<span class="file-text smaller-80"><i class="fa fa-caret-right red"></i>点击或拖拽单张图片</span><br/>							 																	
				<span class="file-name" data-title="No File ..."><i class="fa fa-picture-o"></i></span>																				
			</div>																		
		</div>
    </div>
</div>