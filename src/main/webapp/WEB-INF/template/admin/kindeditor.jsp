<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${CSS}../../kindeditor/kindeditor-all-min.js" charset="utf-8"></script>
<script type="text/javascript" src="${CSS}../../kindeditor/lang/zh-CN.js" charset="utf-8"></script>	
<div class="form-group">
	<label class="col-sm-2 control-label">${param.title}：</label>
	<div class="col-sm-10">
	<textarea name="${param.name}" style="visibility:hidden;width:100%;min-height:300px;" class="kindeditor">${param.value}</textarea>
	<script type="text/javascript">		
		(function(){
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[class="kindeditor"]', {
					resizeType : 1,
					allowPreviewEmoticons : false,
					allowImageUpload : false,
					afterBlur: function(){this.sync();},
					items : [
					         'source', '|', 'undo', 'redo', '|', 'cut', 'copy', 'paste',
					         'plainpaste','|', 'justifyleft', 'justifycenter', 'justifyright',
					         'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
					         'superscript', 'selectall', '|', 'fullscreen',
					         'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
					         'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
					         'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap', 'link', 'unlink'
					],
					uploadJson:"${APP}admin/common/fileupload",
					filePostName:'uploadFileName',
					afterUpload : function(url,data) {	
						if(form){
							form.uploadfile.push(data);
						}
                	}				
				});		
			});		
		})();		
	</script> 
	</div>
</div>