<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="clear"></div>
<div class="article_weizhi">
  <div class="article_weizhi_out jz">
    <p>
    <a href="${APP}">主页</a>
    <c:if test="${null!=menu_title_0}">
    &gt; <a target="_self" href="/mobile/index/category/${menu_title_0.id}">${menu_title_0.title}</a>
    </c:if>
    <c:if test="${null!=menu_title_1}">
    &gt; <a target="_self" href="/mobile/index/category/${menu_title_1.id}">${menu_title_1.title}</a> 
    </c:if>
    &gt;  
    </p>
  </div>
</div>
<div class="article">
   <h3>${article.title}</h3>
   <div class="article-con">${article.details}</div>
   <div class="article_sx jz">
		<div class="clear"></div>
		<c:if test="${null!=pre}">
		<div class="syp">上一篇：<a target="_self" href="/mobile/index/article/${pre.id}">${pre.title}</a> </div>
		</c:if>
		<c:if test="${null!=next}">
		<div class="xyp">下一篇：<a target="_self" href="/mobile/index/article/${next.id}">${next.title}</a> </div>
		</c:if>
		<div class="clear"></div>
   </div>
</div>