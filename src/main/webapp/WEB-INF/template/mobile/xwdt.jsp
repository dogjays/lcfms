<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="clear"></div>
<div class="article_weizhi">
  <div class="article_weizhi_out jz">
   <p>
    <a href="${APP}">主页</a>
    <c:if test="${null!=menu_title_0}">
    &gt; <a target="_self" href="/mobile/index/category?id=${menu_title_0.id}">${menu_title_0.title}</a>
    </c:if>
    <c:if test="${null!=menu_title_1}">
    &gt; <a target="_self" href="/mobile/index/category?id=${menu_title_1.id}">${menu_title_1.title}</a> 
    </c:if>
    &gt;  
    </p>
   </div>
</div>
<div class="list jz">
  <ul class="jz">
     <c:forEach items="${list}" var="vl">
      <li>
      <div class="list_youcejs fr">
        <p><a target="_self" href="/mobile/index/article?id=${vl.id}"><b>${vl.title}</b></a></p>
        <p class="list_nr">${vl.desc}<a href="/mobile/index/article/${vl.id}">[点击详情 ]</a></p>
      </div>
     </li>
     </c:forEach>
  </ul>
  <div class="page" style="margin:15px 0;"> 
  <a target="_self" href="${page.pre()}">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;${page.count()}&nbsp;&nbsp;&nbsp;&nbsp;<a target="_self" href="${page.next()}">下一页</a>
  </div>
</div>