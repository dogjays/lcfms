<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>
	<link rel="stylesheet" href="${CSS}paragraph.css"/>
</head>
<body>
<div class="container-fluid" id="appendHtml">
<div class="row">
<div class="container-fluid">
	<div class="row">
		<div class="paragraph paragraph-success">
			<div class="paragraph-top">
				<button type="button" class="close hide">
					<i class="icon-remove">
					</i>
				</button>
			</div>
			<div class="paragraph-body index">
				<p>
					老成大大
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/29.gif">
					：同学们好！
				</p>
				<p>
					同学们
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/5.gif">
					：老成大大好！
				</p>
				<p>
					老成大大
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/29.gif">
					：同学们辛苦了！
				</p>
				<p>
					同学们
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/5.gif">
					：为代码服务！
				</p>
				<p>
					话说我13年的时候做过一个UI编辑器，比现在的这个炫，可以拖拽，还能在页面上编辑，简直是web版本的MFC，屌暴了有木有，我正美滋滋的等着微软来收购我的作品，一等就是4年，唉。
				</p>
				<p>
					因为，我发现，然并卵。非常非常非常不实用，许多项目做下来，我发现经常性的我并不缺乏整体的布局方案，而常常被细小的功能折腾得筋疲力尽，翻文档，找案例，百度，网页开几十个。举个例子，滚动文字superslide.js的demo我就不止看了十次。
				</p>
				<p>
					这回我下定决心要做一个一篮子解决方案，就是你现在看到的这个神器，我不会告诉你我现在就已经在用了，我正在用“文本段落”的编辑功能来写这个网页，我不仅可以马上看到编辑出来的效果，而且，我终于告别了用代码写文章，刷新看效果的历史，好了，等我写完我就打包下载。更重要的是，这仅仅是这个神器的一丢丢丢丢功能。
				</p>
				<p>
					经常反反复复写的东西，可以不用去找以前的代码来看了，也可以不用去网上搜例子，这个UI神器，旨在解决局部上常用的html代码与脚本，仅仅通过配置，就可以自动生成，是不是很酷呢
					。
				</p>
				<p>
					这么高大上的东西，我可是一分收益都木得，天地良心，我为啥呀，大大我也还是个工薪阶层，正存钱买房了
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/30.gif">
					，收点小费，估计没人愿意，先看看使用反馈，以后我弄个积分，凡是要用的，都得负责推广，至少要让我能放点广告的流量吧。
				</p>
				<p>下载源码：<a target="_blank" href="https://gitee.com/lcfms/lcfms">点击这里</a></p>
				<p>
					加群加群加群，重要的事情说三遍，
					<strong>
						348455534。
					</strong>
				</p>
				<hr/>
				<p style="text-align: center;">
				 广告分割线
				</p>
				<hr/>
				<p>
					媳妇几时有，
				</p>
				<p>
					把酒问青天，
				</p>
				<p>
					不知光棍日子，
				</p>
				<p>
					过到哪一年。
				</p>
				<p>
					我欲摇摇看看，
				</p>
				<p>
					又怕恐龙发现，
				</p>
				<p>
					才子不甚烦。
				</p>
				<p>
					珍爱网，佳缘网，注册完。
				</p>
				<p>
					不应有恨，何事总问房子钱，
				</p>
				<p>
					男有相貌品德，女有美丑善恶，此事古难全。
				</p>
				<p>
					但愿人不丑，千里共网线。
					<img src="https://rescdn.qqmail.com/zh_CN/images/mo/DEFAULT2/20.gif">
				</p>
				<p>
					加微信加微信加微信，重要的事情说三遍，
					<strong>
						13885085112。
					</strong>
				</p>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>
 