<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}chosen.min.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script type="text/javascript" src="${JS}chosen.jquery.min.js"></script> 
	<script src="${JS}showtag.js"></script>
	<style>
		overflow-y: visible;
	</style>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">标题：</label>
	<div class="controls">
		<input placeholder="标题" type="text" name="title" value="单行文字"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">文本类型：</label>
	<div class="controls">
		<select class="width-60 chosen-select tag-input-style" name="style" multiple="multiple" data-placeholder="选择文本类型...">
			<option value="7" selected>非空</option>				
			<option value="11">密码</option>	
			<option value="2">数字</option>
			<option value="3">整数</option>
			<option value="4">正整数</option>	
			<option value="5">非汉字</option>	
			<option value="6">非数字</option>				
			<option value="8">手机号</option>				
			<option value="9">邮箱</option>		
			<option value="12">长度大于5个字符</option>	
			<option value="13">长度大于10个字符</option>
			<option value="14">长度大于20个字符</option>	
			<option value="15">长度小于5个字符</option>	
			<option value="16">长度小于10个字符</option>
			<option value="17">长度小于20个字符</option>	
		</select>
		<span style="color: red;font-size: 10px;">关于长度可以手动修改attribute属性,比如小于100为lg100,大于35为gt35</span>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">name：</label>
	<div class="controls">
		<div class="clearfix">
			<input type="text" name="formname" placeholder="name" value="laocheng"/>
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">placeholder：</label>
	<div class="controls">
		<input placeholder="输入时提示" type="text" name="placeholder" value="输入内容"/>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div> 
<script type="text/javascript">  
		jQuery(function($) {						
			$(".chosen-select").chosen({width:'327px'}); 	
		});
		function saveEdit(){
		var modalElement=getEditHtml();
		var formname=document.Editform.formname.value;
		modalElement.children().find("input:last").attr("name",formname);	
		var placeholder=document.Editform.placeholder.value;
		var title=document.Editform.title.value;
		modalElement.children().find(".control-label:last").html(title+"：");	
		modalElement.children().find("input:last").attr("placeholder",placeholder);	
		var attribute="";			
		var style=$("select[name='style'] option:selected");
		style.each(function(){
		   var val=$(this).val();
		   if(val==2){
				attribute+="number ";		
			}
			if(val==3){
				attribute+="integer ";		
			}
			if(val==4){
				attribute+="+integer ";		
			}
			if(val==5){
				attribute+="unchinaChar ";		
			}
			if(val==6){
				attribute+="unNum ";		
			}
			if(val==7){
				attribute+="unempty ";			
			}
			if(val==8){
				attribute+="mobile ";			
			}
			if(val==9){
				attribute+="email ";			
			}			
			if(val==11){
				modalElement.children().find("input:last").attr("type","password");		
			}
			if(val==12){
				attribute+="gt5 ";			
			}	
			if(val==13){
				attribute+="gt10 ";			
			}	
			if(val==14){
				attribute+="gt20 ";			
			}	
			if(val==15){
				attribute+="lt5 ";			
			}	
			if(val==16){
				attribute+="lt10 ";			
			}	
			if(val==17){
				attribute+="lt20 ";			
			}
		});		
		attribute=trim(attribute);
		modalElement.children().find("input:last").attr("attribute",attribute);
		var size=document.Editform.size.value;
		modalElement.children().find("input:last").css("width",size);
		getEditWindow().form.init("myform");		
		}	
</script>
</body>
</html>

