<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}bootstrap-select.css">
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}bootstrap-select.js"></script> 
</head>
<body>
<div class="container-fluid">
	<div class="row">
	<select class="selectpicker show-tick form-control" data-live-search="true" multiple data-max-options="3">
          <option data-subtext="这是备注">选项1</option>
          <option>选项2</option>
          <option disabled>不让选的</option>
          <optgroup data-subtext="这是一组">
            <option>选项3</option>
            <option selected>选项4</option>
            <option>选项5</option>
          </optgroup>
          <optgroup data-subtext="这是二组">
            <option>选项6</option>
            <option>选项7</option>
            <option>选项8</option>
          </optgroup>
    </select>
	</div>
</div>
</body>
</html>