<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}jquery-ui.min.css"/>	
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>		
	<link rel="stylesheet" href="${CSS}popup.css"/>	
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}jquery-ui.min.js"></script>
	<script src="${JS}popup.js"></script>
</head>
<body>
<div class="container-fluid">
<div class="row">
	<div style="margin: 15px;">
	<a href="javascript:open(${random});">触发弹出层</a>
	<div id="dialog-id-${random}" class="hide" tabindex="0" modal="true" resizable="false" draggable="true" width="380" height="200" toptitle="<h4 class='smaller'><i class='fa fa-edit'></i>弹出层</h4>"></div>									
	</div>
</div>
</div>	  
<script>
	function open(id){
		popup.open(id,function(){
			alert("点击确定的回调方法！");
		},function(){
			alert("点击取消的回调方法！");
		});
	}
</script>
</body>
</html>             