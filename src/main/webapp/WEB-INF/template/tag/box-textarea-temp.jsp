<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}form.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}form.js"></script>
</head>
<body style="padding-top:30px;">
<div class="container-fluid">
<div class="row">
	<div class="form-box form-color-grey">
		<div class="form-header">
			<h6 class="form-title">
				<i class="fa fa-sort"></i>
				表单标题
			</h6>			
		</div>	
		<form class="form-horizontal" name="myform" method="post" action="">
			<div class="form-body" id="appendHtml">
				<!--js_begin-->
				<div class="form-group">
					<label class="col-sm-2 control-label">多行文字：</label>
				    <div class="col-sm-10">
				    	<textarea class="form-control" name="laocheng${random}" rows="5" cols="50"  maxnum="50"></textarea>
				    </div>
				</div>
				<!--js_end-->	
			</div>	
			<div class="form-footer">
				<div class="form-group">
					<label class="col-sm-2"></label>
					<div class="col-sm-10">
						<button type="button" id="save" class="btn btn-sm btn-grey"><i class="fa fa-check"></i> 提交</button>
						<button type="reset" id="reset" class="btn btn-sm btn-grey"><i class="fa fa-undo"></i> 重置</button>
					</div>					
				</div>
			</div>
		</form>
	</div>				
  </div>
</div> 
<script>
var myform=form.init("myform");	
$("#save").click(function(){
	//可以自定义提示正确或错误信息
	//myform.error("表单的name","提示的信息");
	//myform.success("表单的name","提示的信息");
	//可以focus到具体表单
	//myform.focus("表单的name");
	myform.submit();
});
$("#reset").click(function(){
	myform.reset();
});
</script> 
</body>
</html>     

