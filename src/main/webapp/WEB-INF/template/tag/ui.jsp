<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>欢迎使用老成FMS (5.0正式版)-java web框架</title>
		<meta name="keywords" content="java快速开发框架,java编程,java web"> 
	    <meta name="description" content="老成fms(Frame Management System)，是一款前端，后台，业务集成的快速开发框架">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
		<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
		<link rel="stylesheet" href="${CSS}ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="${CSS}ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="${CSS}ace-skins.min.css" />
		<link rel="stylesheet" href="${CSS}ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="${CSS}ace-ie.min.css" />
		<![endif]-->	
		<link rel="stylesheet" href="${CSS}jquery-ui.custom.min.css" />
		<!--[if !IE]> -->
		<script src="${JS}jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->	
		<!--[if IE]>
		<script src="${JS}jquery-1.11.3.min.js"></script>
		<![endif]-->	
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='${JS}jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>	
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}ace-elements.min.js"></script>
		<script src="${JS}ace.min.js"></script>
		<script src="${JS}ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="${JS}html5shiv.min.js"></script>
		<script src="${JS}respond.min.js"></script>
		<![endif]-->
		<script src="${JS}jquery-ui.custom.min.js"></script>
		<script src="${JS}jquery.zclip.min.js"></script>
		<script src="${JS}showtag.js"></script>
</head>
<body class="no-skin">
	<div class="navbar navbar-default" id="navbar">
		<div class="navbar-container" id="navbar-container">
			<div class="navbar-header pull-left">
				<a href="#" class="navbar-brand" style="cursor:default;">
					<small>
						<i class="icon-home"></i>
						老成FMS-UI代码在线生成器1.0 (beta)
					</small>
				</a><!-- /.brand -->
			</div><!-- /.navbar-header -->
			<div class="navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">													
					<li class="green">
						<a href="https://getbootstrap.com/docs/3.3/components/" target="_blank">
							<i class="ace-icon fa fa-tags"></i>
							Glyphicons 图标
						</a>				
					</li>					
					<li class="purple">
						<a href="http://fontawesome.dashgame.com/" target="_blank">
							<i class="ace-icon fa fa-code"></i>
							Fontawesome 图标
						</a>							
					</li>		
				</ul><!-- /.ace-nav -->
			</div><!-- /.navbar-header -->
		</div><!-- /.container -->
	</div>
	<div class="main-container ace-save-state" id="main-container">
		<script type="text/javascript">
			try{ace.settings.loadState('main-container')}catch(e){}
		</script>
		<div id="sidebar" class="sidebar responsive ace-save-state">
			<script type="text/javascript">
				try{ace.settings.loadState('sidebar')}catch(e){}
			</script>				
			<ul class="nav nav-list" id="dragmenu"></ul>
			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<jsp:include page="uiContainer.jsp"/>	
				</div>					
			</div>
		</div><!-- /.main-content -->
	</div><!-- /.main-container -->	
</body>
</html>
