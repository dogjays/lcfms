<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">效果：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="anima" type="radio"  class="ace" value="progress-bar"/>
			<span class="lbl"> 普通</span>
		</label>
		<label>
			<input name="anima" type="radio"  class="ace" value="progress-bar progress-bar-striped"/>
			<span class="lbl"> 条纹</span>
		</label>
		<label>
			<input name="anima" type="radio"  checked class="ace" value="progress-bar progress-bar-striped active"/>
			<span class="lbl"> 动画</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
			<option value="progress-bar-primary" selected>蓝底primary</option>	
			<option value="progress-bar-success">绿底success</option>
			<option value="progress-bar-info">淡蓝info</option>
			<option value="progress-bar-warning">黄底warning</option>
			<option value="progress-bar-danger">红底danger</option>														
		</select>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	function saveEdit(){
	   var modalElement=getEditHtml();
	   var anima=$("input[name=anima]:checked").val();	
	   var style=document.Editform.style.value;	 
	   modalElement.children().find(".progress>div").attr("class",anima+" "+style);
	}
</script>
</body>
</html>
