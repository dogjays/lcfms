<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">显示“<i class="fa fa-check green"></i>”提示：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="show-tick" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="show-tick" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">长度自适应：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="form-control" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="form-control" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">支持多选：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="multiple" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="multiple" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">限制个数：</label>
	<div class="controls">
		<input type="text" name="data-max-options" value="3" placeholder="设置为空，则不限制个数" size="30"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">支持搜索框：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="data-live-search" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="data-live-search" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	function saveEdit(){
	   getEditWindow().$('.selectpicker').selectpicker('destroy');	
	   var modalElement=getEditHtml();	  
	   var c="selectpicker";
	   var showtick=$("input[name=show-tick]:checked").val();	
	   if(showtick==1){
		   c+=" show-tick";
	   }
	   var formcontrol=$("input[name=form-control]:checked").val();	
	   if(formcontrol==1){
		   c+=" form-control";
	   }
	   var m=modalElement.children().find("select");
	   m.attr("class",c);	   	   
	   var multiple=$("input[name=multiple]:checked").val();
	   if(multiple==1){
		   m.attr("multiple",true);
	   }else{
		   m.removeAttr("multiple");
	   }
	   var datamaxoptions=$("input[name=data-max-options]").val();	
	   if(datamaxoptions==''){
		   m.removeAttr("data-max-options");
	   }else{
		   m.attr("data-max-options",datamaxoptions);
	   }
	   var datalivesearch=$("input[name=data-live-search]:checked").val();	
	   if(datalivesearch==1){
		   m.attr("data-live-search",true);
	   }else{
		   m.removeAttr("data-live-search");
	   }
	
	   setTimeout(function(){
		   getEditWindow().$('.selectpicker').selectpicker('render');	
	   },200)
	}
</script>
</body>
</html>
