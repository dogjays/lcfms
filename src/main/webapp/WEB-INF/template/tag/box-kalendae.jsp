<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}chosen.min.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script type="text/javascript" src="${JS}chosen.jquery.min.js"></script> 
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">标题：</label>
	<div class="controls">
		<input placeholder="标题" type="text" name="title" value="选择日历"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">显示月数：</label>
	<div class="controls">
		<div class="clearfix">
			<input type="text" name="months" value="1"/>
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">模型：</label>
	<div class="controls">
		<select class="input-medium" name="mode">
			<option value="single" selected>按日单选</option>
			<option value="range">按日连选</option>
			<option value="multiple">按日多选</option>			
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">禁止选择：</label>
	<div class="controls">
		<div class="radio">
			<label> <input name="direction" type="radio" class="ace" value="future" checked /> <span class="lbl"> 禁止过去</span>
			</label> 
			<label> <input name="direction" type="radio" class="ace" value="past" /> <span class="lbl"> 禁止未来</span>
			</label>
			<label> <input name="direction" type="radio" class="ace" value="" /> <span class="lbl"> 不禁止</span>
			</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div> 
<script type="text/javascript">     
	function saveEdit(){
		  var modalElement=getEditHtml();
		  var title=document.Editform.title.value;
		  modalElement.children().find(".control-label:last").html(title+"：");	
		  var months=document.Editform.months.value;
		  var mode=document.Editform.mode.value;
		  var direction=$("input[name=direction]:checked").val();
		  modalElement.children().find("input:last").attr("months",months);
		  modalElement.children().find("input:last").attr("mode",mode);
		  modalElement.children().find("input:last").attr("direction",direction);
		  getEditWindow().form.init("myform");	
    }

</script>
</body>
</html>

