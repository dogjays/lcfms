<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
</head>
<body style="padding:15px;">
<div class="container-fluid">
<div class="row">
	<div class="progress">
	  <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" style="width:20%;">20%</div>
	</div>
</div>
</div>
</body>
</html>
 