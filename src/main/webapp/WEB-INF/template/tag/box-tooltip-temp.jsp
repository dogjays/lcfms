<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<style>
		body{
			text-align:center;
			padding-top:100px;
		}
	</style>
</head>
<body>
<div class="container-fluid">
<div class="row">
	<span data-toggle="tooltip" data-placement="top" title="这里是提示内容">鼠标放上去</span>
</div>
</div>
<script>
$('[data-toggle=tooltip]').tooltip();
</script>
</body>
</html>
 