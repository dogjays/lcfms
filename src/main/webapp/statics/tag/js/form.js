window.form=(function($){
	var form={};	
	form.uploadfile=[];
	function showSuccess(formname,name,message){
		var b=$(document[formname][name]);
		b.parents(".form-group").removeClass("has-error");
		b.parents(".form-group").addClass("has-success");
		b.nextAll().addClass("hide");
		b.nextAll(".success").removeClass("hide");
		if(message){
			b.nextAll(".success").html("<i class=\"glyphicon glyphicon-ok\"></i> "+message)
		}else{
			b.nextAll(".success").html("")
		}
	}
	function showError(formname,name,message){
		var b=$(document[formname][name]);
		b.parents(".form-group").removeClass("has-success");
		b.parents(".form-group").addClass("has-error");
		b.nextAll().addClass("hide");
		b.nextAll(".error").removeClass("hide");
		if(message){
			b.nextAll(".error").html("<i class=\"glyphicon glyphicon-remove\"></i> "+message)
		}else{
			b.nextAll(".error").html("<i class=\"glyphicon glyphicon-remove\"></i> 出错")
		}
		if(layer){
			layer.msg(message);
		}
	}	
	function checkForm(val,attribute){
			if(!attribute){
				return false;
			}
			if(attribute.indexOf("unempty")!=-1 && val==""){
				return "输入内容不能为空！";
			}
			if(attribute.indexOf(" gt")!=-1){
				var gt=attribute.split(" ");
				var t=0;
				for(var l in gt){					
					var p=parseInt(gt[l].substr(2));				
					if(t<p)t=p;
				}
				if(val.length<t){
					return "输入内容长度必须大于"+t+"！";
				}
			}
			if(attribute.indexOf(" lt")!=-1){
				var lt=attribute.split(" ");
				var t=99999;
				for(var l in lt){					
					var p=parseInt(lt[l].substr(2));				
					if(t>p)t=p;
				}
				if(val.length>t){
					return "输入内容长度必须小于"+t+"！";
				}
			}
			if(attribute.indexOf("number")!=-1 && isNaN(val)){
				return "输入内容必须为数字！";
			}
			if(attribute.indexOf("integer")!=-1){
				var reg1 =/^[-+]?\d*$/;
				if(val.trim().match(reg1) == null){
					return "输入内容必须为整数！";
				}		
			}
			if(attribute.indexOf("+integer")!=-1){
				var reg1 =  /^\d+$/;
				if(val.trim().match(reg1) == null)
				return "输入内容必须为大于0的整数！";
			}
			if(attribute.indexOf("unchinaChar")!=-1){
				var reg1 = /[A-Za-z\d_]+/;  			
				if(val.trim().match(reg1) == null)
				return "输入内容不能为中文！";
			}
			if(attribute.indexOf("unNum")!=-1 && !isNaN(val)){
				return "输入内容不能为数字！";
			}
			if(attribute.indexOf("email")!=-1){	
				var szReg=/^[A-Za-zd]+([-_.][A-Za-zd]+)*@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$/; 
				var c=szReg.test(val); 
				if(!c)
				return "输入内容不是邮件！";
			}
			if(attribute.indexOf("mobile")!=-1){
				var reg = /^1[3|4|5|7|8][0-9]{9}$/; 
				var flag = reg.test(val);
				if(!flag)
				return "输入内容不是手机号！";
			}
			return false;
	}
	function showFocus(formname,name){
		$(document[formname][name]).focus();
	}
	function dropzone(element){			
		var maxFilesize=$(element).attr("maxFilesize");
		var url=$(element).attr("url");
		var maxFiles=$(element).attr("maxFiles");
		var acceptedFiles=$(element).attr("acceptedFiles");	
		$(element).dropzone({								    
			paramName: "uploadFileName",								    
			maxFilesize: maxFilesize,		
			url:url,																		
			addRemoveLinks : true,									
			maxFiles:maxFiles,									
			acceptedFiles:acceptedFiles,											
			dictResponseError: '上传失败!',									
			init: function(){
				this.on("addedfile", function(file){
					
				});
				this.on("removedfile", function(file){
					for(var i=0; i<form.uploadfile.length; i++) {
					    if(form.uploadfile[i] == file) {
					       form.uploadfile.splice(i, 1);
					       break;
					    }
					}	
				});
				this.on("success",function(file, responseText){
					if(!responseText.url){
						alert("一个文件上传时会用uploadFileName这个参数传递给后台,后台完成处理后请回传application/json;charset=utf-8格式的对象！");
						alert("点击提交时,系统会生成多个(根据你上传文件的数目)<input type='hidden' name='uploadFileName' value='传回来的json对象'/>,进行二次提交！");
						alert("一个文件上传成功后请返回一个json,必须包含一个名url的属性，以保存图片的路径，方便二次提交！");
						return;
					}else{
						form.uploadfile.push(responseText);
					}					
				});
			}								 
		});		
	}
	function getNextAreas(element){
		var e=$(element).parent().next().children("select");
		return e;
	}
	function getJsonData(id,element,df){	
		var s=$(element).attr("name");
		var url=$(element).attr("url")+"admin/common/areas?id="+id;
		$.ajax({
	        url: url,
	        dataType: 'json',
	        type:'get',
	        async:false,
	        success: function (res) {
	           if(res.length==0){
	        	   return;
	           }
	           var str="";
	           if(s=='s_province'){
	        	   str+="<option value=\"0\">省份</option>";
	        	   $(getNextAreas(element)).html("<option value=\"0\">地级市</option>"); 
	        	   $(getNextAreas(getNextAreas(element))).html("<option value=\"0\">区、县级市</option>"); 
	        	   $(getNextAreas(getNextAreas(getNextAreas(element)))).html("<option value=\"0\">街道</option>");
	           }
	           if(s=='s_city'){
	        	   str+="<option value=\"0\">地级市</option>";
	        	   $(getNextAreas(element)).html("<option value=\"0\">区、县级市</option>"); 
	        	   $(getNextAreas(getNextAreas(element))).html("<option value=\"0\">街道</option>");
	           }
			   if(s=='s_county'){
				   str+="<option value=\"0\">区、县级市</option>";
				   $(getNextAreas(element)).html("<option value=\"0\">街道</option>");
	           }
			   if(s=='s_street'){
				   str+="<option value=\"0\">街道</option>";
	           }
	           for(var i=0;i<res.length;i++){
	        	   if(df && res[i].id==df){
	        		   str+="<option selected value=\""+res[i].id+"\">"+res[i].name+"</option>";
	        	   }else{
	        		   str+="<option value=\""+res[i].id+"\">"+res[i].name+"</option>";
	        	   }       	  		        	
	           }
	           $(element).html(str);
	        }
	    });	
	}
	var SelectedForm=function(name){
		this.name=name;
		this.submit;
		this.error;
		this.success;
		this.focus;
	}
	form.init=function(formname){	
		$(".form-group .success").remove();
		$(".form-group .error").remove();
		$(".form-group").removeClass("has-success");
		$(".form-group").removeClass("has-error");	
		var o=new SelectedForm(formname);	
		$("form[name='"+formname+"'] .form-control").each(function(){			
			var attribute=$(this).attr("attribute");
			var name=$(this).attr("name");				
			var str="<span class=\"help-block success hide\"><i class=\"glyphicon glyphicon-ok\"></i></span>";
			$(this).after(str);	
			var str="<span class=\"help-block error hide\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
			$(this).after(str);
			if(this.tagName=="INPUT" && this.type=="text"){
				$(this).blur(function(){	
					var val=$(this).val();
					var r=checkForm(val,attribute);
					if(r!==false){
						showError(formname,name,r);
					}else{
						showSuccess(formname,name);
					}
				});	
			}
			if(this.tagName=="INPUT" && this.type=="password"){
				$(this).blur(function(){	
					var val=$(this).val();
					var r=checkForm(val,attribute);
					if(r!==false){
						showError(formname,name,r);
					}else{
						showSuccess(formname,name);
					}
				});	
			}
            if(this.tagName=="TEXTAREA"){
				    var maxnum=$(this).attr("maxnum");
					if(maxnum!=0){
						$(this).keyup(function(){						
							var val=$(this).val();
							if(val.length>maxnum){
								showError(formname,name,"超出了"+(val.length-maxnum)+"个字");
							}else{
								showSuccess(formname,name,"还能输入"+(maxnum-val.length)+"个字");
							}
						}); 						
					}               									
			}
			if(this.tagName=="INPUT" && this.type=="radio"){
					
			}
			if(this.tagName=="INPUT" && this.type=="checkbox"){
							
			}	
			if(this.tagName=="SELECT" && this.name=="s_province"){
				getJsonData($(this).attr("parent"),this,$(this).attr("default"));				
				$("select[name='s_province']").change(function(){
					var id=$(this).val();	
					var e=getNextAreas(this);
					if(e.length>0)getJsonData(id,e,$(this).attr("default")); 									   
				});
			}		
			if(this.tagName=="SELECT" && this.name=="s_city"){
				if($(this).attr("parent")){
					getJsonData($(this).attr("parent"),this,$(this).attr("default"));	
				}
				$("select[name='s_city']").change(function(){
					var id=$(this).val();
					var e=getNextAreas(this);
					if(e.length>0)getJsonData(id,e,$(this).attr("default")); 
				});
			}
			if(this.tagName=="SELECT" && this.name=="s_county"){
				if($(this).attr("parent")){
					getJsonData($(this).attr("parent"),this,$(this).attr("default"));	
				}		
				$("select[name='s_county']").change(function(){
					var id=$(this).val();	
					var e=getNextAreas(this);
					if(e.length>0)getJsonData(id,e,$(this).attr("default")); 									   
				});				
			}
			if(this.tagName=="SELECT" && this.name=="s_street"){
				if($(this).attr("parent")){
					getJsonData($(this).attr("parent"),this,$(this).attr("default"));	
				}									
			}
			if(this.tagName=="INPUT" && $(this).hasClass('calendar')){
				var k=document.getElementsByClassName('calendar');
					for(var i1=0;i1<k.length;i1++){
						var months=$(k[i1]).attr('months');
						var mode=$(k[i1]).attr('mode');
						var direction=$(k[i1]).attr('direction');
						$(k[i1]).attr('id','Kalendae'+i1);
						new Kalendae.Input('Kalendae'+i1, {
							months:months,
							mode:mode,
							direction:direction
					});		
				}	
			}		
		});	
		
		$(".dropzone").each(function(){
			dropzone(this);
		});
		
		return o;
	}
	//提交
	SelectedForm.prototype.submit=function(){	
		var formname=this.name;
		var f=true;
		$("form[name='"+formname+"'] .form-control").each(function(){
			var attribute=$(this).attr("attribute");
			var val=$(this).val();
			var name=$(this).attr("name");	
			var r=checkForm(val,attribute);
			if(r!==false){
				showError(formname,name,r);	
				showFocus(formname,name);
				f=false;
			}		
		});		
		if(f){
			if(form.uploadfile.length>0){
				for(var i=0;i<form.uploadfile.length;i++){
					var input=document.createElement("input");
					input.name="uploadFileName";
					input.type="hidden";
					input.value=JSON.stringify(form.uploadfile[i]);
					$(document[this.name]).append(input);
				}	
				var input=document.createElement("input");
				input.name="uploadFileName";
				input.type="hidden";
				input.value="{id:0,type:\"none\",url:\"这里是为了防止springmvc提交映射报错,根据需要可以删除\"}";
				$(document[this.name]).append(input);
			}
			var method=$(document[this.name]).attr("method");
			if(method=='ajax' || method=='AJAX'){
				var action=$(document[this.name]).attr("action");
				var data=$("form[name='myform'] input,textarea,select");
				var postdata={};
				for(var i=0;i<data.length;i++){
					if(data[i].type=='radio' && !data[i].checked){
						continue;
					}
					if(data[i].type=='checkbox' && !data[i].checked){
						continue;
					}
					if(data[i].name && !postdata[data[i].name]){
						postdata[data[i].name]=[];
						postdata[data[i].name].push(data[i].value);
					}else if(data[i].name && postdata[data[i].name]){
						postdata[data[i].name].push(data[i].value);
					}else{
						continue;
					}									
				}
				for(var m in postdata){
					if(postdata[m].length==1){
						postdata[m]=postdata[m][0];
					}
				}
				$.ajax({
					url: action,
			        dataType: 'json',
			        type:'post',
			        data:postdata,
			        traditional:true,
			        success: function (res) {
			        	if(layer && res.msg){
			        		layer.msg(res.msg);
			        	}			        	
			        	if(res.code==1 && res.url){
                            if(parent.layer){
                            	setTimeout(function(){
    			        			var index = parent.layer.getFrameIndex(window.name);
    				        		parent.layer.close(index);
    				        		parent.location.href=res.url;
    			        		},1000);
                            }else{
                            	setTimeout(function(){
    				        		location.href=res.url;
    			        		},1000);
                            }	
			        	}			        	
			        }
				});
			}else{
				document[this.name].submit();
			}		
		}	
	}
	//报错
	SelectedForm.prototype.error=function(name,message){
		showError(this.name,name,message);
	}
	//重置
	SelectedForm.prototype.reset=function(){
		form.uploadfile=[];
		form.init(this.name);
	}
	//提示成功
	SelectedForm.prototype.success=function(name,message){
		showSuccess(this.name,name,message);
	}
	//获取焦点
	SelectedForm.prototype.focus=function(name){
		showFocus(this.name,name);
	}
	//获取值
	SelectedForm.prototype.getValue=function(name){
		return document[this.name][name].value;
	}
	//判断有无错误
	SelectedForm.prototype.hasError=function(){
		var b=$(document[this.name]).children().find(".has-error");
		if(b.length>0){
			return true;
		}
		return false;
	}
	
	return form;
})($)
