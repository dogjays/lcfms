package cn.lcfms.app.index.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.lcfms.app.admin.controller.AdminBaseController;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonPageView;

@Controller("index.IndexController")
@RequestMapping("/index/index")
public class IndexController extends AdminBaseController{	
	/*
	 * 首页
	 */
	@RequestMapping("/init")
	public CommonPageView init(){	
		BaseService service=App.getService("demo");
		service.column("id");
		service.column("i as 测试i");
		service.column("s as 测试s",(Object object)->{
			String s=(String)object;
			if(s.equals("aaab")) {
				return "<a id='adddddfd'>弹出框</a>";
			}else{
				return object;
			}
		});
		CommonPageView view =new CommonPageView("测试",service);
		view.setFilterInputEq("查下S", "s", "");
		view.setSort("测试s");
		view.setUpdateTab("abc?id=", 68);
		view.setEditUrl("打快点", "fa-plus", "abc?id=");
		return view;
	}
	
}
