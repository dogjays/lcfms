package cn.lcfms.app.mobile.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.StaticHtml;
import cn.lcfms.bin.core.App;
import cn.lcfms.utils.Page;
import cn.lcfms.utils.StringUtils;

@Controller("mobile.IndexController")
@RequestMapping("/mobile/index")
public class IndexController extends MobileBaseController{
	
	@RequestMapping("/init")
	@StaticHtml(file="/index.html")
	public ModelAndView init(HttpServletRequest request,HttpServletResponse response) throws IOException {
		ModelAndView view=new ModelAndView("index");
		BaseService service = App.getService("articles");
		service.column("id","left(title,12) as title","labels").where("category_id=25").limit(0, 9).order("id desc");
		List<HashMap<String, Object>> jkzs = service.selectList();
		service.column("id","left(title,12) as title","labels").where("category_id=26").limit(0, 9).order("id desc");
		List<HashMap<String, Object>> mtjj = service.selectList();
		service.column("id","left(title,12) as title","labels").where("category_id=27").limit(0, 9).order("id desc");
		List<HashMap<String, Object>> yydt = service.selectList();
		service.column("id","left(title,6) as title","labels").where("`category_id`=19 AND `mid`=27").limit(0, 9).order("id desc");
		List<HashMap<String, Object>> kfal = service.selectList();
		view.addObject("jkzs", jkzs);
		view.addObject("mtjj", mtjj);
		view.addObject("yydt", yydt);
		view.addObject("kfal", kfal);		
		return view;
	}
	
    @RequestMapping("/search/{keywords}")
	public ModelAndView category(
			@PathVariable(name="keywords",required=false) String keywords,
			HttpServletRequest request,
			HttpServletResponse response) 
    {	
    	ModelAndView view=new ModelAndView("xwdt");
    	BaseService service = App.getService("articles");
    	service.whereLike("desc", keywords);
    	int page=StringUtils.StringToInteger(request.getParameter("page"), 1);
    	List<HashMap<String, Object>> list = service.selectPage(10, page);
    	request.setAttribute("list", list);
		if(list.size()>0) {
			Long count=(Long)list.get(0).get("count");
			Page obj=new Page(count.intValue(), 10, 5, request);
			request.setAttribute("page", obj);
		}	
		HashMap<String, Object> map = service.setTable("categories").where("id=24").column("id","topid","title").selectMap();
		request.setAttribute("menu_title_1", map);
    	return view;   				
	}
    
    @RequestMapping("/category/{id}")
	public ModelAndView category_id(@PathVariable int id,HttpServletRequest request,HttpServletResponse response) {	
    	return Cms("categories",id,request,response);			
	}
	
	@RequestMapping("/article/{id}")
	public ModelAndView article_id(@PathVariable int id,HttpServletRequest request,HttpServletResponse response) {	
		return Cms("articles",id,request,response);
	}
	
	@RequestMapping("/category")
	public ModelAndView category(int id,HttpServletRequest request,HttpServletResponse response) {	
    	return Cms("categories",id,request,response);			
	}
	
	@RequestMapping("/article")
	public ModelAndView article(int id,HttpServletRequest request,HttpServletResponse response) {	
		return Cms("articles",id,request,response);
	}
	
	@RequestMapping("/baiduditu")
	public ModelAndView baiduditu() {	
		ModelAndView view=new ModelAndView("baiduditu");
		return view;
	}
	
}
