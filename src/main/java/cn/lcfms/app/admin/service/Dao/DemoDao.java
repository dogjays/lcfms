package cn.lcfms.app.admin.service.Dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.lcfms.app.admin.bean.DemoBean;

public interface DemoDao {	
	public DemoBean getDemo(int id);	
	public List<HashMap<String, Object>> getPage(@Param(value = "0") int pagerow,@Param(value = "1") int page);
}
