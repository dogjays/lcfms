package cn.lcfms.app.admin.bean;

public class DemoBean {
	private int id;
	private int i;
	private String s;
	private String t;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public String getT() {
		return t;
	}
	public void setT(String t) {
		this.t = t;
	}
	
	private String changeDian(String str) {		
		if(str.indexOf("as")!=-1) {
			String[] split = str.split("as");
			return changeDian(split[0].trim())+" "+split[1].trim();
		}
		if(str.indexOf("AS")!=-1) {
			String[] split = str.split("AS");
			return changeDian(split[0].trim())+" "+split[1].trim();
		}
		if(str.indexOf(" ")!=-1) {
			String[] split = str.split(" ");
			return changeDian(split[0].trim())+" "+split[1].trim();
		}
		if(str.indexOf(".")!=-1) {
			String[] split = str.split("\\.");
			return split[0]+".`"+split[1]+"`";
		}
		return str;
	}
	public static void main(String[] args) {
		String string=new DemoBean().changeDian("table a");
		System.out.println(string);
	}
	
}
