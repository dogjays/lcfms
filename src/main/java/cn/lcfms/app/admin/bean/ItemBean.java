package cn.lcfms.app.admin.bean;

public class ItemBean{
	private String icon="fa-angle-double-right";
	private String permitId;
	private String url;
	private int itemId;
	private String itemName;
	private int sort;
	private int parentId;
	private String parentName;

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getPermitId() {
		return permitId;
	}
	public void setPermitId(String permitId) {
		this.permitId = permitId;
	}
}
