package cn.lcfms.app.admin.controller;



import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.Userinfo;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.annotation.StaticHtml;
import cn.lcfms.bin.core.App;

@PermitPoll(type="系统功能",name="后台框架",forward="/login.html")
@Controller("admin.IndexController")
@RequestMapping("/admin/index")
public class IndexController extends AdminBaseController{	
	
	@RequestMapping("/ace")
	public ModelAndView ace(HttpServletRequest request){
		int firstitemid=(int) App.APPCONFIG.get("ace.firstitemid");
		Userinfo.putUserInfo("firstitemid", firstitemid, request);
		ModelAndView view=new ModelAndView("admin/ace");	
		return view;
	}
	
	@RequestMapping("/first")
	@StaticHtml(file="/WEB-INF/cache/file/first.html")
	public ModelAndView first(HttpServletRequest request){
		ModelAndView view=new ModelAndView("admin/first");					
		return view;
	}	
	
	@RequestMapping("/clearCache")
	@ResponseBody
	public String clearCache(){
		BaseCache.ITEMCACHE=new ArrayList<>();
		BaseCache.HTML=new ArrayList<>();
		BaseCache.TABLEINFO=new HashMap<>();
		BaseCache.PERMIT=new ArrayList<>();
		return "1";
	}	
	
	@RequestMapping("test")
	@ResponseBody
	public String test(HttpServletRequest request) {
		return "1111";
	}	
}
