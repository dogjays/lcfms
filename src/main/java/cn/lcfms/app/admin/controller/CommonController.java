package cn.lcfms.app.admin.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import net.coobird.thumbnailator.Thumbnails;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.App;
import cn.lcfms.utils.TimeUtils;
import cn.lcfms.utils.TimeUtils.Formatter;


@Controller("admin.CommonController")
@RequestMapping("/admin/common")
public class CommonController extends AdminBaseController{
	
	@RequestMapping("areas")
	public String areas(int id){
		BaseService service=App.getService("areas");
		service.where("parent_id="+id);
		service.column("id","name");
		List<HashMap<String, Object>> list = service.selectList();
		JSONArray array=JSONArray.fromObject(list);
		return array.toString();
	}
	
	@RequestMapping("fileupload")
	public String fileupload(String dir,@RequestParam("uploadFileName") MultipartFile file,HttpServletRequest request) throws IOException{		
		if(file.getSize()>0){
			String filefullname=file.getOriginalFilename();	
			int index=filefullname.lastIndexOf(".");
			//文件老名字
			String alt="";
			//文件类型
			String filetype="";
			//文件新名字
			String filename="";
			//扩展名
			String extension="";
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMM");
			Date date = new Date();
			//日期文件夹
			String fold=sdf.format(date);
			String s=File.separator;
			//存储的文件夹
			String path="";
			if(index!=-1){
				alt=filefullname.substring(0, index);
				extension=filefullname.substring(index).toLowerCase();
			}else{
				alt=filefullname;
			}
			if(extension.equals(".png") || 
			   extension.equals(".jpg") || 
			   extension.equals(".jpeg")||
			   extension.equals(".bmp") ||
			   extension.equals(".gif")
			  ){
				path=App.ROOT_PATH+s+"uploadfile"+s+"images"+s+fold+s;
				filetype="img";
			}
			if(extension.equals(".mp3")){
				path=App.ROOT_PATH+s+"uploadfile"+s+"music"+s+fold+s;
				filetype="music";
			}
			Random r=new Random();
			String ri = String.valueOf(r.nextInt(1000));
			filename=String.valueOf(date.getTime()).substring(3,13)+ri+extension;
			File f=new File(path+filename);
			File fo=new File(path);		
			if(!fo.exists()){
				fo.mkdirs();
			}		
			f.createNewFile();
			file.transferTo(f);
			if(filetype.equals("img")){	
				File fs=new File(path+"thumb");
				if(!fs.exists()){
					fs.mkdirs();
				}
			}		
			long id=fileInsertSql(filetype,filename,alt,fold);			
			if(filetype.equals("img")){			
				String u= "{\"error\":0,\"url\":\""+(String)request.getAttribute("APP")+"uploadfile/images/"+fold+"/"+filename+"\",\"type\":\"img\",\"id\":"+id+"}";				
				return u;
			}
			if(filetype.equals("music")){
				String u= "{\"error\":0,\"url\":\""+(String)request.getAttribute("APP")+"uploadfile/music/"+fold+"/"+filename+"\",\"type\":\"music\",\"id\":"+id+"}";				
				return u;
			}
		}
		return "{success:0}";
	}
	
	/**
	 * 
	 * @param filetype 文件类型
	 * @param filename alt名称,非文件名
	 * @param filepath http路径
	 * @param filerealpath 物理路径
	 */
	private long fileInsertSql(String filetype,String filename,String alt,String fold){
		String s=File.separator;
		int r=0;
		BaseService service=App.getService("files");
		switch (filetype) {
		case "img":			
			String thumbpath=App.ROOT_PATH+s+"uploadfile"+s+"images"+s+fold+s+"thumb"+s+filename;
			String thumbhttp=fold+"/thumb/"+filename;
			String photopath=App.ROOT_PATH+s+"uploadfile"+s+"images"+s+fold+s+filename;
			String photohttp=fold+"/"+filename;
			boolean b = operateByMaxSize(photopath,thumbpath);
			if(!b){
				thumbhttp=photohttp;
			}
			service.setData("img",alt,photohttp,thumbhttp,TimeUtils.getCurrentDateTime(Formatter.SHORT));
			service.insert("type","name","original","thumbnail","created_at");	
			r=service.insert_id();
			break;
		case "music":		
			break;
		case "video":
			break;
		case "file":
			break;
		default:
			break;
		}
		return r;
	}
	//保持宽度250px不变,等比例压缩图片
	private boolean operateByMaxSize(String inSrc,String outSrc){  
        boolean flag = false;  
        try {  
        	BufferedImage bi = ImageIO.read(new File(inSrc));
        	int m=(250*bi.getHeight())/bi.getWidth();        	   	
        	Thumbnails.of(bi).size(250, m).toFile(outSrc);
            flag = true;  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return flag;  
    }  
	
}
