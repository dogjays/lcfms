package cn.lcfms.app.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.app.BaseController;
import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.Broadcast;
import cn.lcfms.bin.Userinfo;
import cn.lcfms.bin.core.App;
import cn.lcfms.utils.Encrypt;
import cn.lcfms.bin.view.Common;

public class AdminBaseController extends BaseController{	
	/**
	 * 该方法将在执行控制器之前被执行
	 * @throws Exception 
	 */
    @Override
    protected void beforeProtected(HttpServletRequest request,HttpServletResponse response){
    	super.beforeProtected(request,response);
    	String basePath=(String) request.getAttribute("APP");
    	request.setAttribute("BASEPATH", basePath); 
    	request.setAttribute("JS", basePath+"statics/tag/js/"); 
    	request.setAttribute("CSS", basePath+"statics/tag/css/"); 
    	request.setAttribute("IMG", basePath+"statics/tag/images/");   
    	if(BaseCache.ITEMCACHE.size()==0){
    		set_item_cache();
    	}
    }  
    /**
   	 * 该方法将在执行控制器之后，调入模板之前被执行
     * @throws IOException 
   	 */
    @Override
    protected void afterProtected(HttpServletRequest request,HttpServletResponse response,ModelAndView view) throws IOException{  	
    	super.afterProtected(request,response,view);    
		setBroadcastId(request);
    	if(view!=null && view.getViewName()!=null){	
    		String viewname=view.getViewName();	
    		if(viewname.startsWith("local:")){
    			view.setViewName("admin/local");
    			view.addObject("LOCAL", viewname.substring(6));	
    		}
			if(viewname.startsWith("admin/table/")){
				CommonPage(request,view);
			}						
		}
    }
	
	private void CommonPage(HttpServletRequest request, ModelAndView view) {
		Map<String, Object> model = view.getModel();
		Common common=(Common) model.get("CommonPage");
		common.setPage(request);
	}
	
	//初始化所有栏目
	protected void set_item_cache(){
		BaseService service = App.getService("item");
		List<HashMap<String, Object>> list=service.order("sort").selectList();
		for(int i=0;i<list.size();i++){
			int itemId=(int) list.get(i).get("itemId");
			for(int m=0;m<list.size();m++){
				int parentId=(int) list.get(m).get("parentId");
				if(parentId==itemId){
					list.get(i).put("hasChild", true);
					break;
				}
			}
		}		
		BaseCache.ITEMCACHE=list;	
	}
	
    //设置用户通知id
    private void setBroadcastId(HttpServletRequest request){		
		int broadid = App.getBroadcastId();
		Encrypt md5=Encrypt.getEncrypt();	
		if(null==Userinfo.getUserInfo("aid", request)) {
			return;
		}
		int userId=(int) Userinfo.getUserInfo("aid", request);
		String userName=(String) Userinfo.getUserInfo("aname", request);
		Userinfo.putUserInfo("broadid", md5.md5(String.valueOf(userId)+userName+broadid), request);	
	}
    //成功消息
    protected void Success(String message,HttpServletRequest request) {
		int aid=(int) Userinfo.getUserInfo("aid", request);
		try {
			Broadcast.echo(aid, "{\"content\":\""+message+"\",\"style\":\"alert-success\"}");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    //失败消息
    protected void Error(String message,HttpServletRequest request) {
		int aid=(int) Userinfo.getUserInfo("aid", request);
		try {
			Broadcast.echo(aid, "{\"content\":\""+message+"\",\"style\":\"alert-danger\"}");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
