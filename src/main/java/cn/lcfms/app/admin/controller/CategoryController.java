package cn.lcfms.app.admin.controller;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.lcfms.app.admin.bean.CategoryBean;
import cn.lcfms.app.admin.service.DataService;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonEditView;
import cn.lcfms.bin.view.CommonPageView;
import cn.lcfms.utils.TimeUtils;

@PermitPoll(type="CMS管理",name="栏目管理",forward="/nopermit.html")
@Controller("admin.CategoryController")
@RequestMapping("/admin/category")
public class CategoryController extends AdminBaseController{
	
	@Autowired
	DataService g_service;
	
	@RequestMapping("/addCategory")
	public CommonEditView addCategory(){
		CommonEditView view=new CommonEditView("增加栏目");
		view.setInputForm("栏目名称", "title", "", "unempty");	
		view.addObject("pTitle", "父");
		view.setViewForm("frame/categories.jsp");			
		view.setSelectForm("首页展示", "is_top_showing", "是,否", "1,0", "0");	
		String[] model = g_service.getModel();
		view.setSelectForm("网页模型", "mid", "无,"+model[0], "-1,"+model[1], "");		
		view.setUploadImageForm("栏目图片");	
		view.setTextareaForm("栏目描述", "desc", "", 200);
		view.setFormAction("saveCategory");		
		return view;
	}
	
	@RequestMapping("/updateCategory")
	public CommonEditView updateCategory(int id){
		BaseService service=App.getService("categories");
		HashMap<String, Object> map = service.where("id=#{id}").setData(id).selectMap();
		CommonEditView view=new CommonEditView("修改栏目");
		view.setHiddenForm("id", id);
		view.setInputForm("栏目名称", "title", map.get("title"), "unempty");	
		view.addObject("pTitle", "父");
		int topid = (int) map.get("topid");
		HashMap<Integer, String> category = g_service.getCategory(topid);
		view.setViewForm("frame/categoriesEdit.jsp");
		view.addObject("categoryId", category.keySet());	
		Object object = map.get("is_top_showing");
		if(null==object || "".equals(object) || !(boolean)object) {
			view.setSelectForm("首页展示", "is_top_showing", "是,否", "1,0", "0");
		}else {
			view.setSelectForm("首页展示", "is_top_showing", "是,否", "1,0", "1");
		}		
		String[] model = g_service.getModel();
		view.setSelectForm("网页模型", "mid", "无,"+model[0], "-1,"+model[1], map.get("mid"));	
		view.setUploadImageForm("栏目图片");	
		view.setTextareaForm("栏目描述", "desc", map.get("desc"), 200);
		view.setFormAction("saveCategory");		
		return view;
	}
	
	@RequestMapping("saveCategory")
	public String saveCategory(CategoryBean cate,int[] category_ids){
		int topid=0;
		if(category_ids != null) {
			for(int i=0;i<category_ids.length;i++){			
				if(topid<category_ids[i]){
					topid=category_ids[i];
				}
			}
		}
		cate.setTopid(topid);
		cate.setCreated_at(TimeUtils.getCurrentDateTime());
		g_service.saveCategory(cate);
		return "local:catelist";
	}
	
	@RequestMapping("/deteCategory")
	public String deteCategory(int id){			
		BaseService service=App.getService("categories");
		service.deleteById(id);
		return "local:catelist";
	}
	
	@RequestMapping("/getCategory")
	@ResponseBody
	public String getCategory(int id){			
		BaseService service=App.getService("categories");
		service.where("topid=#{topid}");
		if(id==0){
			service.setData("");
		}else{
			service.setData(id);
		}		
		service.column("id","title");
		List<HashMap<String, Object>> list = service.order("sort").selectList();
		JSONArray array=JSONArray.fromObject(list);
		return array.toString();
	}
	
	
	@RequestMapping("/catelist")
	public CommonPageView catelist(HttpServletRequest request){
		BaseService service=App.getService("categories a");	
		service.leftJoin("categories b").on("a.topid=b.id");
		service.leftJoin("model c").on("a.mid=c.mid");
		service.column("a.id as id","a.id as 栏目ID");
		service.column("a.id as 排序",(Object object)->{	
			int id=(int)object;
			return "<a href='javascript:up("+id+");'><i class='fa fa-arrow-up'></i> 向上</a>&nbsp;&nbsp;<a href='javascript:down("+id+");'><i class='fa fa-arrow-down'></i> 向下</a>";
		});
		service.column("a.title as 栏目名称","b.title as 上级栏目");	
		service.column("(CASE a.is_top_showing WHEN 1 THEN '<span class=\"label-success label\">是</span>' END) AS 首页展示");
		service.column("c.template as 模板");
		if(null!=request.getParameter("id")){
			String id=request.getParameter("id");
			service.where("a.topid="+id);
		}
		service.order("a.sort");
		CommonPageView view=new CommonPageView("栏目管理",service);
		view.setToolBarUrl("添加栏目", "fa-plus", "addCategory");
		view.setFilterInputLike("名称", "a.title", "");		
		view.setDeleteUrl("deteCategory?id=");
		view.setUpdateUrl("updateCategory?id=");		
		view.setEditTab("查看内容", "fa-share-square-o", "list?cid=", 193);
		view.setEditUrl("子栏目", "fa-hand-o-right ", "catelist?id=");
		view.setWidgetBottomJsp("frame/upDown.jsp");
		return view;		
	}
	
	@RequestMapping("/updown")
	public String updown(String action,int id) {
		BaseService service=App.getService("categories");	
		if(action.equals("up")) {
			service.setData(id).sql("update categories set sort=sort-1 where id=#{id}");
		}else if(action.equals("down")) {
			service.setData(id).sql("update categories set sort=sort+1 where id=#{id}");
		}
		return "local:catelist";
	}
	
}
