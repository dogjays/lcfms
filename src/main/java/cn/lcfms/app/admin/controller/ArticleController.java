package cn.lcfms.app.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.lcfms.app.admin.bean.ArticleBean;
import cn.lcfms.app.admin.service.CommonService;
import cn.lcfms.app.admin.service.DataService;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonEditView;
import cn.lcfms.bin.view.CommonPageView;

@PermitPoll(type="CMS管理",name="文章管理",forward="/nopermit.html")
@Controller("admin.ArticleController")
@RequestMapping("/admin/article")
public class ArticleController extends AdminBaseController{
	
	@Autowired
	CommonService c_service;
	
	@Autowired
	DataService g_service;
	
	
	@RequestMapping("/list")
	public CommonPageView list(HttpServletRequest request){
		BaseService service=App.getService();	
		service.setTable("categories");
		List<HashMap<String, Object>> list = service.column("id","title").selectList();
		StringBuffer text=new StringBuffer();
		StringBuffer values=new StringBuffer();
		for(int i=0;i<list.size();i++) {
			int id = (int) list.get(i).get("id");
			String title = (String) list.get(i).get("title");
			text.append(title);
			values.append(id);
			if(i!=list.size()-1) {
				text.append(",");
				values.append(",");
			}
		}
		service.setTable("articles a");		
		service.leftJoin("categories b").on("a.category_id=b.id");
		service.leftJoin("model c").on("a.mid=c.mid");
		service.column("a.id as id","a.id as 文章ID","a.title as 标题","b.title as 栏目");	
		service.column("c.name as 模型");
		service.column("c.template as 模板");		
		if(null!=request.getParameter("cid")){
			String cid=request.getParameter("cid");
			service.where("a.category_id="+cid);
		}
		service.order("a.id desc");
		CommonPageView view=new CommonPageView("文章管理",service);
		view.setToolBarUrl("添加内容", "fa-plus", "addArticle");
		view.setDeleteUrl("deleteArticle?id=");
		view.setUpdateUrl("editArticle?id=");
		view.setFilterInputLike("标题", "a.title", "");		
		view.setFilterSelectEq("栏目", "a.category_id", ","+text.toString(), ","+values.toString(), "");
		view.setColumnWidth("标题", 600);
		return view;
	}
	
	@RequestMapping("listJson")
	public String listJson(HttpServletRequest request){
		BaseService service=App.getService("articles");			
		service.column("id","name");
		String keywords=request.getParameter("keywords");
		if(null!=keywords){
			service.where("name like '%"+keywords+"%'");					
		}
		service.limit(0, 10);
		List<HashMap<String, Object>> list = service.selectList();
		JSONArray array=JSONArray.fromObject(list);
		return array.toString();
	}
	
	@RequestMapping("/deleteArticle")
	public String deleteArticle(int id){
		BaseService service=App.getService("articles");
		service.deleteById(id);
		return "local:list";
	}
	
	@RequestMapping("/editArticle")
	public CommonEditView editArticle(int id){		
		CommonEditView view=new CommonEditView("修改内容");
		BaseService service=App.getService("articles");
		HashMap<String, Object> map = service.setData(id).where("id=#{id}").selectMap();
		int category_id = (int) map.get("category_id");
		HashMap<Integer, String> category = g_service.getCategory(category_id);
		view.addObject("categoryId", category.keySet());
		view.setHiddenForm("id", id);
		view.setInputForm("标题", "title", map.get("title"),"unempty");	
		view.setViewForm("frame/categoriesEdit.jsp");
		String[] model = g_service.getModel();
		view.setSelectForm("模型", "mid", model[0], model[1], map.get("mid"));	
		view.setInputForm("标签", "labels", map.get("labels"), "","请输入标签，多个标签用英文逗号隔开");	
		view.setUploadImageForm("图片");
		view.setTextareaForm("摘要", "desc", map.get("desc"), 100);
		view.setEditorForm("内容", "details", map.get("details"));		
		view.setFormAction("saveEdit");
		return view;
	}
	
	@RequestMapping("/addArticle")
	public CommonEditView addArticle(){		
		CommonEditView view=new CommonEditView("增加内容");
		view.setInputForm("标题", "title", "", "unempty");		
		view.setViewForm("frame/categories.jsp");
		String[] model = g_service.getModel();
		view.setSelectForm("模型", "mid", model[0], model[1], "");		
		view.setInputForm("标签", "labels", "", "","请输入标签，多个标签用英文逗号隔开");	
		view.setUploadImageForm("图片");
		view.setTextareaForm("摘要", "desc", "", 100);
		view.setEditorForm("内容", "details", "");		
		view.setFormAction("saveEdit");		
		return view;
	}
	
	@RequestMapping("/saveEdit")
	public String saveEdit(ArticleBean article,HttpServletRequest request){
		int[] is = article.getCategory_ids();
		int category_id=0;
		if(is!=null) {
			for(int i=0;i<is.length;i++){			
				if(category_id<is[i]){
					category_id=is[i];
				}
			}
		}
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		article.setCreated_at(sdf.format(date));
		article.setCategory_id(category_id);
		boolean b = g_service.saveArticle(article);
		if(b) {
			Success("保存成功", request);
		}
		return "local:list";
	}
	
}
