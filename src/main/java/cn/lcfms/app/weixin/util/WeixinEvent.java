package cn.lcfms.app.weixin.util;

import net.sf.json.JSONObject;

public class WeixinEvent {
	/*
	 * 扫一扫事件处理,完毕后将结果发送给用户，url则跳转
	 */
	public static String scancode_push(WeixinMsg weixinMsg, String eventKey,JSONObject ScanCodeInfo) {
		return "success";
	}
	/*
	 * 扫一扫事件处理，完毕后关闭照相机
	 */
	public static String scancode_waitmsg(WeixinMsg weixinMsg, String eventKey,JSONObject ScanCodeInfo) {
		return "success";
	}
	/*
	 * 拍照事件处理
	 */
	public static String pic_sysphoto(WeixinMsg weixinMsg, String eventKey,JSONObject SendPicsInfo) {
		return "success";
	}
	/*
	 * 拍照或获取系统相册事件处理
	 */
	public static String pic_photo_or_album(WeixinMsg weixinMsg, String eventKey, JSONObject sendPicsInfo) {
		return "success";
	}
	/*
	 * 获取微信相册事件处理
	 */
	public static String pic_weixin(WeixinMsg weixinMsg, String eventKey, JSONObject sendPicsInfo) {
		return "success";
	}
	/*
	 * 获取地址事件处理
	 */
	public static String location_select(WeixinMsg weixinMsg, String eventKey,JSONObject SendLocationInfo) {
		return "success";
	}
	
}
