package cn.lcfms.bin.view;



import javax.servlet.http.HttpServletRequest;


public class CommonEditLayer extends CommonEdit{
	public CommonEditLayer(){
		this.setViewName("admin/table/editlayer");
		this.addObject("CommonPage", this);
		this.setMethodAjax();
	}

	@Override
	public void setPage(HttpServletRequest request) {
		request.setAttribute("form", form);
		request.setAttribute("action", action);
		request.setAttribute("method", method);
	}
}
