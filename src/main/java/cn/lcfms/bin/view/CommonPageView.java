package cn.lcfms.bin.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.Userinfo;
import cn.lcfms.utils.StringUtils;

public class CommonPageView extends ModelAndView implements Common{
	protected int pageSize=15;//每页多少行
	protected int pageNumber=1;//当前页
	protected String title;
	protected HashSet<String> sortColumn;
	protected HashSet<HashMap<String, Object>> filter;
	protected String sort;
	protected BaseService service;
	protected HashSet<HashMap<String, Object>> edit;
	protected String widgetTopJsp;
	protected String widgetBottomJsp;
	protected String tableTopJsp;
	protected String tableBottomJsp;
	protected HashSet<HashMap<String, Object>> toolbar;
	protected HashMap<String, Integer> columnWidth;
	protected String[] column;
	protected boolean actionButton;
	protected String primaryKey;
	/**
	 * @param act 操作类型,使用CommonView常量定义
	 * @param table 表名
	 * @param title 操作名称
	 */
	public CommonPageView(String title){
		this.title=title;
		this.addObject("CommonPage", this);
		this.setViewName("admin/table/selectpage");
		setDefaultToolbar();
	}
	
	public CommonPageView(String title,BaseService service){
		this.title=title;
		this.service=service;	
		this.addObject("CommonPage", this);
		this.setViewName("admin/table/selectpage");
		setDefaultToolbar();
		if(null!=service.getSqlBuild().table) {
			this.init();
		}		
	}
	//设置默认的toolbar
	private void setDefaultToolbar() {
		this.toolbar=new HashSet<>();
		HashMap<String, Object> map2=new HashMap<>();
		map2.put("action", "fullscreen");
		map2.put("icon", "fa-expand");
		map2.put("type", "js");
		toolbar.add(map2);
		HashMap<String, Object> map3=new HashMap<>();
		map3.put("action", "collapse");
		map3.put("icon", "fa-chevron-up");
		map3.put("type", "js");
		toolbar.add(map3);
		HashMap<String, Object> map4=new HashMap<>();
		map4.put("action", "close");
		map4.put("icon", "fa-minus");
		map4.put("type", "js");
		toolbar.add(map4);
	}
	/**
	 * 设置列表的列
	 * @param column
	 */
	public void setColumn(String...column) {
		this.column=column;
	}
	/**
	 * @param act 操作类型,使用CommonView常量定义
	 * @param table 表名
	 * @param title 操作名称
	 */
	private void init(){			
		//设置默认的修改删除两个按钮
		this.edit=new HashSet<>();
		HashMap<String, Object> map=new HashMap<>();
		map.put("name", "修改");
		map.put("icon", "fa-edit");
		map.put("type","url");
		map.put("action","edit?id=");
		map.put("primaryKey", getPrimaryKey());
		edit.add(map);
		HashMap<String, Object> map1=new HashMap<>();
		map1.put("name", "删除");
		map1.put("icon", "fa-trash");
		map1.put("type","url");
		map1.put("action","delete?id=");
		map1.put("primaryKey", getPrimaryKey());
		edit.add(map1);		
		//默认设置主键不显示
		setColumnWidth(getPrimaryKey(), 0);	
		//默认设置显示操作
		this.actionButton=true;
	}
	
	/*
	 * 获取主键
	 */
	private String getPrimaryKey() {
		if(null!=primaryKey) {
			return primaryKey;
		}
		if(null!=service && null!=service.getSqlBuild().table) {
			return service.getPrimaryKey();
		}
		return "id";
	}
	/**
	 * 设置主键
	 * @param key
	 */
	public void setPrimaryKey(String key) {
		this.primaryKey=key;
	}
	
	/**
	 * 设置列表列宽
	 * @param column
	 * @param width
	 */
	public void setColumnWidth(String column,int width){
		if(columnWidth==null){
			columnWidth=new HashMap<String, Integer>();
		}
		columnWidth.put(column, width);
	}
	
	/**
	 * 设置每页显示的行数
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 隐藏操作按钮
	 */
	public void hiddenActionButton() {
		this.actionButton=false;
	}
	/**
	 * 设置参与排序的字段
	 * @param string
	 */
	public void setSort(String... string) {
		if(sortColumn==null){
			sortColumn=new HashSet<String>();
		}
		for(int i=0;i<string.length;i++){
			sortColumn.add(string[i]);
		}		
	}	
	/**
	 * 设置修改的超链接
	 * @param action
	 * @param primaryKey
	 */
	public void setUpdateUrl(String action) {
		Iterator<HashMap<String, Object>> iterator = edit.iterator();
		while(iterator.hasNext()){
			HashMap<String, Object> next = iterator.next();
			if(next.get("name").equals("修改")){
				if(null==action || action.equals("")){
					edit.remove(next);
					break;
				}else{
					next.put("action",action);
					next.put("primaryKey",getPrimaryKey());
				}			
			}
		}
	}
	/**
	 * 设置新tab弹出修改页面
	 */
	public void setUpdateTab(String action,int itemId) {
		Iterator<HashMap<String, Object>> iterator = edit.iterator();
		while(iterator.hasNext()){
			HashMap<String, Object> next = iterator.next();
			if(next.get("name").equals("修改")){
				if(null==action || action.equals("")){
					edit.remove(next);
					break;
				}else{
					next.put("action",action);
					next.put("primaryKey",getPrimaryKey());
					next.put("type","tab");
					next.put("itemId",itemId);
					List<HashMap<String, Object>> itemcache = BaseCache.ITEMCACHE;
					check:for(int i=0;i<itemcache.size();i++) {
						int iid = (int) itemcache.get(i).get("itemId");
						if(itemId==iid) {
							String itemName = (String) itemcache.get(i).get("itemName");
							next.put("itemName",itemName);
							break check;
						}
					}
				}			
			}
		}
	}
	/**
	 * 设置新layer窗口弹出修改页面
	 * @param action
	 * @param primaryKey
	 * @param itemId
	 */
	public void setUpdateLayer(String action,int width,int height) {
		Iterator<HashMap<String, Object>> iterator = edit.iterator();
		while(iterator.hasNext()){
			HashMap<String, Object> next = iterator.next();
			if(next.get("name").equals("修改")){
				if(null==action || action.equals("")){
					edit.remove(next);
					break;
				}else{
					next.put("type","layer");
					next.put("action",action);
					next.put("primaryKey",getPrimaryKey());
					next.put("width",width);
					next.put("height",height);
				}			
			}
		}
	}
	/**
	 * 设置执行删除的超链接
	 * @param action
	 * @param primaryKey
	 */
	public void setDeleteUrl(String action) {
		Iterator<HashMap<String, Object>> iterator = edit.iterator();
		while(iterator.hasNext()){
			HashMap<String, Object> next = iterator.next();
			if(next.get("name").equals("删除")){
				if(null==action || action.equals("")){
					edit.remove(next);
					break;
				}else{
					next.put("action",action);
					next.put("primaryKey",getPrimaryKey());
				}			
			}
		}
	}
	/**
	 * 设置一个提醒弹出框的删除
	 * @param action
	 * @param primaryKey
	 */
	public void setDeleteLayer(String action) {
		Iterator<HashMap<String, Object>> iterator = edit.iterator();
		while(iterator.hasNext()){
			HashMap<String, Object> next = iterator.next();
			if(next.get("name").equals("删除")){
				if(null==action || action.equals("")){
					edit.remove(next);
					break;
				}else{
					next.put("type","layer");
					next.put("action",action);
					next.put("primaryKey",getPrimaryKey());
					next.put("width",0);
					next.put("height",0);
				}			
			}
		}
	}
	
	/**
	 * 添加按钮,效果超链接跳转
	 * @param name
	 * @param icon
	 * @param action
	 * @param primaryKey
	 * @param itemId 
	 */
	public void setEditUrl(String name,String icon,String action){
		HashMap<String, Object> map=new HashMap<>();
		map.put("name", name);
		map.put("icon", icon);
		map.put("action",action);
		map.put("type","url");
		map.put("primaryKey",getPrimaryKey());
		edit.add(map);
	}
	/**
	 * 添加按钮,效果为弹出个layer层
	 * @param action
	 * @param primaryKey
	 * @param width
	 * @param height
	 */
	public void setEditLayer(String name,String icon,String action,int width,int height) {
		HashMap<String, Object> map=new HashMap<>();
		map.put("name", name);
		map.put("icon", icon);
		map.put("action",action);
		map.put("type","layer");
		map.put("primaryKey",getPrimaryKey());
		map.put("width",width);
		map.put("height",height);
		edit.add(map);
	}
	/**
	 * 添加按钮,效果为新的tab框
	 * @param name
	 * @param icon
	 * @param action
	 * @param primaryKey
	 * @param itemId
	 */
	public void setEditTab(String name,String icon,String action,int itemId){
		HashMap<String, Object> map=new HashMap<>();
		map.put("name", name);
		map.put("icon", icon);
		map.put("action",action);
		map.put("type","tab");
		map.put("primaryKey",getPrimaryKey());
		List<HashMap<String, Object>> itemcache = BaseCache.ITEMCACHE;
		check:for(int i=0;i<itemcache.size();i++) {
			int iid = (int) itemcache.get(i).get("itemId");
			if(itemId==iid) {
				String itemName = (String) itemcache.get(i).get("itemName");
				map.put("itemName",itemName);
				map.put("itemId",itemId);
				break check;
			}
		}
		edit.add(map);
	}
	
	/**
	 * 添加input且等于类型的过滤
	 * @param title 标题
	 * @param name 字段名
	 * @param value 默认值
	 */
	public void setFilterInputEq(String title,String name,String value){
		if(this.filter==null){
			this.filter=new HashSet<HashMap<String, Object>>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);
		map.put("form", "input");
		map.put("type", "eq");
		filter.add(map);
		int size = filter.size();
		map.put("sort", size-1);
	}
	/**
	 * 添加input且like类型的过滤
	 * @param title 标题
	 * @param name 字段名
	 * @param value 默认值
	 */
	public void setFilterInputLike(String title,String name,String value){
		if(this.filter==null){
			this.filter=new HashSet<HashMap<String, Object>>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();		
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);
		map.put("form", "input");
		map.put("type", "like");
		filter.add(map);
		int size = filter.size();
		map.put("sort", size-1);
	}
	/**
	 * 添加input且like类型的过滤
	 * @param title 标题
	 * @param name 字段名
	 * @param text 下拉菜单显示的文字,多个值用逗号隔开如:学生,老师,校长
	 * @param values 下拉菜单显示的值,多个值用逗号隔开如:1,2,3
	 * @param value 默认值
	 */
	public void setFilterSelectEq(String title,String name,String text,String values,String value){
		if(this.filter==null){
			this.filter=new HashSet<HashMap<String, Object>>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("value", value);		
		map.put("text", text.split(","));
		map.put("values", values.split(","));		
		map.put("form", "select");
		map.put("type", "eq");
		filter.add(map);
		int size = filter.size();
		map.put("sort", size-1);
	}
	/**
	 * 添加时间类型的过滤
	 * @param title 标题
	 * @param name 字段名
	 * @param time 时间
	 */
	public void setFilterTime(String title,String name,String time) {
		if(this.filter==null){
			this.filter=new HashSet<HashMap<String, Object>>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("name", name);
		map.put("time", time);	
		map.put("form", "time");
		map.put("type", "eq");
		filter.add(map);
		int size = filter.size();
		map.put("sort", size-1);
	}
	
	//自定义widget顶部的jsp
	public void setWidgetTopJsp(String widgetTopJsp) {
		this.widgetTopJsp = widgetTopJsp;
	}
	//自定义widget底部的jsp
	public void setWidgetBottomJsp(String widgetBottomJsp) {
		this.widgetBottomJsp = widgetBottomJsp;
	}
	//自定义table顶部的jsp
	public void setTableTopJsp(String tableTopJsp) {
		this.tableTopJsp = tableTopJsp;
	}
	//自定义table底部的jsp
	public void setTableBottomJsp(String tableBottomJsp) {
		this.tableBottomJsp = tableBottomJsp;
	}
	//设置toolbar
	public void setToolBar(String action,String icon) {
		HashMap<String, Object> map=new HashMap<>();
		map.put("action", action);
		map.put("icon", icon);
		map.put("type", "js");
		toolbar.add(map);
	}
	//设置toolbar为弹出个layer层
	public void setToolBarUrl(String title,String icon,String url) {
		HashMap<String, Object> map=new HashMap<>();
		map.put("icon", icon);
		map.put("url", url);
		map.put("type", "url");
		map.put("title", title);
		toolbar.add(map);
	}
	//设置toolbar为弹出个layer层
	public void setToolBarLayer(String title,String icon,String url,int width,int height) {
		HashMap<String, Object> map=new HashMap<>();
		map.put("icon", icon);
		map.put("title", title);
		map.put("type", "layer");
		map.put("url", url);
		map.put("width", width);
		map.put("height", height);
		toolbar.add(map);
	}
	//设置toolbar为跳转到新的tab页
	public void setToolBarTab(String title,String icon,String url,int itemId) {
		HashMap<String, Object> map=new HashMap<>();
		map.put("icon", icon);
		map.put("type", "tab");
		map.put("title", title);
		map.put("url", url);
		List<HashMap<String, Object>> itemcache = BaseCache.ITEMCACHE;
		check:for(int i=0;i<itemcache.size();i++) {
			int iid = (int) itemcache.get(i).get("itemId");
			if(itemId==iid) {
				String itemName = (String) itemcache.get(i).get("itemName");
				map.put("itemName",itemName);
				map.put("itemId",itemId);
				break check;
			}
		}
		toolbar.add(map);
	}
	
	public List<HashMap<String, Object>> setPageData(HttpServletRequest request) {
		if(null==service) {
			return new ArrayList<>();
		}
		if(null!=request.getParameter("pageNumber")){
			pageNumber=StringUtils.StringToInteger(request.getParameter("pageNumber"), 1);
		}
		if(null!=request.getParameter("pageSize")){
			pageSize=StringUtils.StringToInteger(request.getParameter("pageSize"), pageSize);
		}	
		if(null!=request.getParameter("sort_asc")){
			String sort=request.getParameter("sort_asc");
			service.order(sort+" asc");
			this.sort=sort+"_asc";			
		}	
		if(null!=request.getParameter("sort_desc")){
			String sort=request.getParameter("sort_desc");
			service.order(sort+" desc");
			this.sort=sort+"_desc";
		}	
		if(null!=filter){
			Iterator<HashMap<String, Object>> iterator = filter.iterator();			
			HashMap<String, Object> where=new HashMap<>();
			while(iterator.hasNext()){
				HashMap<String, Object> next = iterator.next();
				String name=(String) next.get("name");
				String type=(String) next.get("type");
				String value = request.getParameter(name);	
				String lname=(name.indexOf(".")==-1)?name:name.substring(name.indexOf(".")+1, name.length());
				if(null!=value && !"".equals(value) && type.equals("eq")){
					service.where(name+"=#{"+lname+"}");
					where.put(lname, value);		
					next.put("value", value);
				}
				if(null!=value && !"".equals(value) && type.equals("like")){
					service.where(name+" like #{"+lname+"}");
					where.put(lname, "%"+value+"%");	
					next.put("value", value);
				}
			}
			service.setData(where);
		}
		List<HashMap<String, Object>> datalist=service.selectPage(pageSize, pageNumber);
		if(null==column) {
			column = service.getSqlBuild().column;		
			for(int i=0;i<column.length;i++){
				String string=column[i].trim();
				if(string.indexOf(" ")!=-1){	
					string=string.substring(string.lastIndexOf(" ")+1);		
				}
				column[i]=string;
			}	
		}	
		return datalist;
	}
	
	public int setPageCount(HttpServletRequest request) {
		return 0;
	}
	
	@Override
	public void setPage(HttpServletRequest request){	
		List<HashMap<String, Object>> datalist=setPageData(request);
		int total=0;
		if(datalist.size()>0 && datalist.get(0).containsKey("count")) {
			if(datalist.get(0).get("count") instanceof Long) {
				Long l = (Long) datalist.get(0).get("count");
				total=l.intValue();
			}else {
				total=(int) datalist.get(0).get("count");
			}					
		}else {
			total=setPageCount(request);
		}				
		request.setAttribute("filter", this.filter);
		request.setAttribute("edit", edit);
		request.setAttribute("column",column);
		request.setAttribute("columnWidth",columnWidth);
		request.setAttribute("title", title);
		request.setAttribute("sortColumn", sortColumn);
		request.setAttribute("sort", sort);
		request.setAttribute("tableTopJsp", tableTopJsp);
		request.setAttribute("tableBottomJsp", tableBottomJsp);
		request.setAttribute("widgetTopJsp", widgetTopJsp);
		request.setAttribute("widgetBottomJsp", widgetBottomJsp);
		request.setAttribute("pageurlpath",setUrlPath(request));
		request.setAttribute("pageurlquery",setUrlQuery(request));
		request.setAttribute("pagenumber",pageNumber);
		request.setAttribute("pagesize", pageSize);
		request.setAttribute("totalrow", total);
		request.setAttribute("datalist", datalist);
		request.setAttribute("toolbar", toolbar);
		request.setAttribute("actionButton", actionButton);
	}	
	
	private String setUrlPath(HttpServletRequest request){
		String[] mca=(String[]) Userinfo.getUserInfo("mca", request);
		String r=mca[0]+"/"+mca[1].toLowerCase()+"/"+mca[2];
		return r;
	}
	
	private String setUrlQuery(HttpServletRequest request){
		if(filter==null && sort==null){
			return "";
		}
		String r="";
		if(filter!=null){
			Iterator<HashMap<String, Object>> iterator = filter.iterator();			
			while(iterator.hasNext()){
				HashMap<String, Object> next = iterator.next();
				String name=(String) next.get("name");
				String value=(String) next.get("value");		
				if(null!=value && !"".equals(value)){
					r+=name+"="+value+"&";
				}		
			}
		}		
		if(sort!=null){
			if(sort.endsWith("_asc")){
				r+="sort_asc="+sort.substring(0, sort.indexOf("_asc"));
			}
			if(sort.endsWith("_desc")){
				r+="sort_desc="+sort.substring(0, sort.indexOf("_desc"));
			}
		}		
		if(r.endsWith("&")){
			r=r.substring(0, r.length()-1);
		}	
		if(!r.equals("")){
			r="?"+r;
		}
		return r;
	}
}
