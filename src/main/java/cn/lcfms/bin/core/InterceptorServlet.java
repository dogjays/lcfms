/*
 * 控制器拦截器,处理权限问题
 * 作者：成绍勇
 * 2015-11-9
 */
package cn.lcfms.bin.core;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.Permission;
import cn.lcfms.bin.Userinfo;



public class InterceptorServlet extends HandlerInterceptorAdapter {
	private static Logger logger = Logger.getLogger(InterceptorServlet.class); 
	
	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex)throws Exception {		
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		System.out.println(2222);
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	/**
     * 执行模板之前访问
     * @param handler
     * @return
     */
	@Override
	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		/*************调用模板之前执行*************/
		this.afterParentAction(handler,request,response,modelAndView);
		super.postHandle(request, response, handler, modelAndView);
	}
	
	/**
	 * 当执行完成action之后将调入的方法
	 * @param handler
	 * @param request
	 * @param response
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private void afterParentAction(Object handler,HttpServletRequest request,HttpServletResponse response,ModelAndView modelAndView) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = null ;
		Object bean =((HandlerMethod)handler).getBean();
		for (Class<?> clazz = bean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod("afterProtected",HttpServletRequest.class,HttpServletResponse.class,ModelAndView.class);
				break;
			} catch (Exception e) {
			}
		}
		if(method!=null){
			method.setAccessible(true);
			method.invoke(bean,request,response,modelAndView);
		}	
		
	}
	/**
	 * 为父类的setProtected属性或方法初始化
	 * @param handler
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private void beforeParentAction(Object handler,HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Method method = null ;	
		Object bean =((HandlerMethod)handler).getBean();	
		for (Class<?> clazz = bean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod("beforeProtected",HttpServletRequest.class,HttpServletResponse.class);
				break;
			} catch (Exception e) {				
			}
		}
		if(method!=null){
			method.setAccessible(true);
			method.invoke(bean,request,response);
		}	
	}

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {			
		boolean debug=(boolean) App.APPCONFIG.get("debug.on");
		if(debug){
			debugUserInfo(request,response);	
		}
		Permission permission=Permission.getpermission();;
		//初始化权限
    	if(BaseCache.PERMIT.size()==0 && !debug){
        	permission.initialize();
        	permission.set_permission(); 	
    	}
    	if(BaseCache.PERMIT.size()==0 && debug){
        	permission.debugInitialize();
        	permission.set_permission(); 	
    	}
    	String[] mca = (String[]) Userinfo.getUserInfo("mca", request);
	    String m=mca[0];
	    String c=mca[1];
	    String a=mca[2];
    	InterceptorServlet.getUserInfo(request, response);
		try {
			//0是未设置权限，1是为类设置了权限，2是为方法设置了权限，3是all开放
			int if_permit=0;	    
		    Iterator<HashMap<String, Object>> iterator=BaseCache.PERMIT.iterator();
		    while (iterator.hasNext()) {
		    	HashMap<String, Object> map=iterator.next();
				String action=(String)map.get("action");
				String forward=(String)map.get("forward");
				if(action.equals("cn.lcfms.app."+m+".controller."+c+"Controller."+a)){
					String type=(String)map.get("type");
					if(!type.equals("all")){
						if_permit=2;
						Userinfo.putUserInfo("per_forward", forward, request);
						Userinfo.putUserInfo("perActionID",map.get("id"), request);
						break;
					}else{
						if_permit=3;
					}		
				}
			}
		    iterator=BaseCache.PERMIT.iterator();
		    if(if_permit==0){
		    	while (iterator.hasNext()) {
			    	HashMap<String, Object> map=iterator.next();
					String action=(String)map.get("action");
					String forward=(String)map.get("forward");
					if(action.equals("cn.lcfms.app."+m+".controller."+c+"Controller")){
						if_permit=1;
						Userinfo.putUserInfo("per_forward", forward, request);
						Userinfo.putUserInfo("perActionID",map.get("id"), request);
						break;
					}
				}
		    }
		    //该控制器设置有权限，查看权限后访问
		    if(if_permit==1 || if_permit==2){
		    	//获取用户信息
		    	boolean permit = InterceptorServlet.do_permission(request, response,permission);			
				if(permit){// 用户有权限
					logger.info("通过拦截");
					/*载入静态页面*/
					boolean b=ShowStaticHtml(request,response);
					if(b) {
						return false;
					}
					beforeParentAction(handler,request,response);
					return super.preHandle(request, response, handler);
				}else{	
					logger.info("用户被拦截");
					return false;
				}	
		    //该控制器没设置权限，直接访问
		    }else{
		    	logger.info("没有设置权限，直接进入");
				/*载入静态页面*/
				boolean b=ShowStaticHtml(request,response);
				if(b) {
					return false;
				}
				beforeParentAction(handler,request,response);
		    	return super.preHandle(request, response, handler);
		    }	
		} catch (Exception e) {
			e.printStackTrace();	
			return false;
		}	
	}
	
	/*
	 * 判断并跳转到静态页面
	 */
	private boolean ShowStaticHtml(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, IOException {		
    	String[] mca = (String[]) Userinfo.getUserInfo("mca", request);
	    String m=mca[0];
	    String c=mca[1];
	    String a=mca[2];
    	HashMap<String, Object> cache = DefaultPageFilter.html(m, c, a,request);
    	if(cache==null) {
    		return false;
    	}else {
    		logger.info("查看静态页面");
    		String file=(String) cache.get("file");
    		boolean overdue=(boolean) cache.get("overdue");
    		File file2=new File(App.ROOT_PATH+file);
    		if(file2.exists() && !overdue) {
    			logger.info("获取并发送静态页面");
    			String htmlstr = FileUtils.readFileToString(file2,"utf8");
    			PrintWriter writer = response.getWriter();
    			response.setContentType("text/html;charset=utf8");
        		writer.write(htmlstr);
        		writer.flush();
        		writer.close();
    		}else {
    			logger.info("生成且发送静态页面");
    			Userinfo.putUserInfo("html", App.ROOT_PATH+file, request);
    			return false;
    		}   		
    		return true;
    	}		
	}

	/*
	 * 获得用户sessonId,并将用户信息保存到USERINFO这个常量中
	 */
	private static void getUserInfo(HttpServletRequest request,HttpServletResponse response){	
		HttpSession session=request.getSession();
		session.setMaxInactiveInterval(3600);
		Userinfo.putUserInfo("JSESSIONID", session.getId(), request);
		Enumeration<String> names=session.getAttributeNames();
		while(names.hasMoreElements()){
			String name=names.nextElement();
			Userinfo.putUserInfo(name, session.getAttribute(name), request);			
		}  
		logger.info("用户:"+Userinfo.getUserInfo("aname", request)+",用户Id:"+Userinfo.getUserInfo("aid", request));
	}	
	/*
	 * debug环境下设置用户信息
	 */
	private static void debugUserInfo(HttpServletRequest request,HttpServletResponse response){	
		int aid=(int) App.APPCONFIG.get("debug.aid");
		String aname=(String) App.APPCONFIG.get("debug.aname");
		int gid=(int) App.APPCONFIG.get("debug.gid");	
		HttpSession session=request.getSession();
		session.setMaxInactiveInterval(3600);
		Userinfo.putUserInfo("JSESSIONID", session.getId(), request);
		Userinfo.putUserInfo("aid", aid, request);
		Userinfo.putUserInfo("aname", aname, request);
		Userinfo.putUserInfo("gid", gid, request);
		Enumeration<String> names=session.getAttributeNames();
		while(names.hasMoreElements()){
			String name=names.nextElement();
			Userinfo.putUserInfo(name, session.getAttribute(name), request);			
		}  
	}	
	//判断用户权限
	private static boolean do_permission(HttpServletRequest request,HttpServletResponse response,Permission permission) throws IOException{
		boolean if_permit = permission.if_permisson(request);
		if (!if_permit) {
			String forward = (String)Userinfo.getUserInfo("per_forward", request);
			if(forward.startsWith("/")) {
				String contextPath = request.getContextPath();
			    if(request.getServerPort()==80){
			    	forward = request.getScheme()+"://"+request.getServerName()+contextPath+forward;
			    }else{
			    	forward = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+forward;
			    }			    
			}
			response.sendRedirect(forward);		
			return false;
		}
		return true;
	}	
}


