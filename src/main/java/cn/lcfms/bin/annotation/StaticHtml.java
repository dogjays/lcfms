package cn.lcfms.bin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/*为单个控制器生成静态页面缓存注解*/
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface StaticHtml {
	//缓存时间
	int time() default 3600;
	//外加参数,只支持长度小于10的字符串,如果为数字,将以字符串类型处理,长度如果超过将取前10位
	String[] param() default {};
	String[] dynamicParam() default {};
	//设置缓存文件路径,使用${param}可以实现动态替换param中的值
	String file() default "";
}