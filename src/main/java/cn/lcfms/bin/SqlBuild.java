package cn.lcfms.bin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import cn.lcfms.bin.BaseService.ColumnFormat;

public class SqlBuild{
	public String sql;
	public String table;
	public String alias;
	public String action;
	public List<HashMap<String, Object>> result=new ArrayList<>();;
	public int affected_rows;
	public Object params;
	public HashMap<String, Object> map=new HashMap<>();
	public Object insert_id;
	public String keyType;
	public boolean success;
	public String[] column;
	public String primaryKey;
	public HashSet<String> where;
	public HashSet<String> whereOr;
	public String limit;
	public String order;
	public String groupBy;
	public HashSet<String> join;
	public HashMap<String,ColumnFormat> formats;
	public String having;

	
	public void buildSql() {		
		switch (action) {
		case "insert":
			buildInsert();
			break;
		case "update":
			buildUpdate();
			break;
		case "delete":
			buildDelete();
			break;
		case "select":
			buildSelect();
			break;
		case "selectPage":
			buildSelectPage();
			break;
		default:
			break;
		}
	}
	
	private void buildSelectPage() {
		buildSelect();
		String countsql="SELECT COUNT(1) AS count ";
		countsql+=sql.substring(sql.indexOf("FROM"));
		if(sql.indexOf("LIMIT")!=-1) {
			countsql=countsql.substring(0, countsql.indexOf(" LIMIT"));
		}
		if(sql.indexOf("ORDER BY")!=-1) {
			countsql=countsql.substring(0, countsql.indexOf(" ORDER BY"));
		}
		sql="SELECT * FROM ("+countsql+") t1,("+sql+") t2";
	}

	private void buildSelect() {	
		sql="SELECT ";
		for(int i=0;i<column.length;i++){	
			String cl=column[i].trim();
			cl=changeDian(cl);
			if(i==0){
				sql+=cl;
			}else{
				sql+=","+cl;
			}				
		}
		if(null==alias) {
			sql+=" FROM `"+table+"`";
		}else {
			sql+=" FROM `"+table+"` "+alias;
		}
		setJoin();
		setWhere();
		if(null!=groupBy) {
			sql+=" GROUP BY "+changeOrder(groupBy);
		}
		if(null!=having) {
			sql+=" HAVING "+having;
		}
		if(null!=order) {
			order=order.trim();
			if(order.indexOf("DESC")!=-1 || order.indexOf("ASC")!=-1 ) {
				sql+=" ORDER BY "+changeOrder(order);
			}else {
				sql+=" ORDER BY "+changeOrder(order)+" ASC";
			}		
		}
		if(null!=limit) {
			sql+=" LIMIT "+limit;
		}
	}
	
	private void setJoin() {
		if(null!=join) {
			Iterator<String> iterator = join.iterator();
			while(iterator.hasNext()){
				String next = iterator.next().trim();
				sql+=" "+next;	
			}		
		}
	}

	private void buildDelete() {
		sql="DELETE FROM `"+table+"`";	
		setWhere();
	}

	private void buildUpdate() {
		sql="UPDATE `"+table+"` SET ";	
		for(int i=0;i<column.length;i++){
			if(i==0){
				if(column[i].indexOf("=")!=-1){
					sql+=column[i];	
				}else{
					sql+="`"+column[i]+"`=#{"+column[i]+"}";	
				}							
			}else{	
				if(column[i].indexOf("=")!=-1){
					sql+=","+column[i];		
				}else{
					sql+=",`"+column[i]+"`=#{"+column[i]+"}";
				}						
			}
		}	
		setWhere();
	}
	
	private void setWhere() {
		if(null!=where) {
			Iterator<String> iterator = where.iterator();
			int i=0;
			while(iterator.hasNext()){
				String next = iterator.next().trim();
				next=changeWhere(next);
				if(i==0){
					sql+=" WHERE ("+next+")";
				}else {
					sql+=" AND ("+next+")";
				}			
				i++;
			}		
		}	
		if(null!=whereOr && where.size()>0) {
			Iterator<String> iterator = whereOr.iterator();
			while(iterator.hasNext()){
				String next =  iterator.next().trim();
				next=changeWhere(next);
				sql+=" OR ("+next+")";		
			}	
		}
	}
	
	private String changeWhere(String str) {
		String regex="\\w+=";
		Pattern pattern=Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()){
			String p=matcher.group();
			str=str.replaceFirst(p, "`"+p.substring(0, p.length()-1)+"`=");
		}
		return str;
	}
	

	private void buildInsert() {
		boolean b=false;
		for(int i=0;i<column.length;i++) {
			if(column[i].equals(primaryKey)) {
				b=true;
				break;
			}
		}
		if(b) {
			sql="INSERT INTO `"+table+"` (";	
		}else {
			sql="INSERT INTO `"+table+"` (`"+primaryKey+"`,";	
		}
		for(int i=0;i<column.length;i++){
			if(i==column.length-1){
				sql+="`"+column[i];	
			}else {
				sql+="`"+column[i]+"`,";	
			}			
		}
		if(b) {
			sql+="`) VALUES (";
		}else {
			sql+="`) VALUES (#{"+primaryKey+"},";
		}	
		for(int i=0;i<column.length;i++){
			if(i==column.length-1){
				sql+="#{"+column[i]+"})";
			}else{
				sql+="#{"+column[i]+"},";
			}		
		}	
	}
	
	private String changeDian(String str) {
		if(str.indexOf(" as ")!=-1) {
			String[] split = str.split(" as ");
			return changeDian(split[0].trim())+" AS "+split[1].trim();
		}
		if(str.indexOf(" AS ")!=-1) {
			String[] split = str.split(" AS ");
			return changeDian(split[0].trim())+" AS "+split[1].trim();
		}
		if(str.indexOf(".")!=-1) {
			String regex="\\.\\w+";
			Pattern pattern=Pattern.compile(regex);
			Matcher matcher = pattern.matcher(str);
			while(matcher.find()){
				String p=matcher.group();
				str=str.replaceFirst(p, ".`"+p.substring(1, p.length())+"`");
			}
		}			
		return changeColumn(str);
	}
	
	private String changeOrder(String order) {
		String[] split = order.split(",");
		StringBuffer buffer=new StringBuffer();
		for(int i=0;i<split.length;i++) {
			String str=split[i];
			if(str.indexOf(".")!=-1) {
				String regex="\\.\\w+";
				Pattern pattern=Pattern.compile(regex);
				Matcher matcher = pattern.matcher(str);
				while(matcher.find()){
					String p=matcher.group();
					str=str.replaceFirst(p, ".`"+p.substring(1, p.length())+"`");
				}
			}else if(str.indexOf(".")==-1 && str.indexOf(" ")==-1) {
				str="`"+str+"`";
			}else if(str.indexOf(" ")!=-1) {
				str="`"+str.substring(0,str.indexOf(" "))+"`"+str.substring(str.indexOf(" "));
			}			
			buffer.append(str);
			if(i!=split.length-1) {				
				buffer.append(",");
			}
		}
		return buffer.toString();
	}
	
	private String changeColumn(String cl) {
		//没有点也没有空格,单个字段
		if(cl.indexOf(".")==-1 && cl.indexOf(" ")==-1) {			
			if(cl.indexOf("(")==-1 && cl.indexOf(")")==-1 && cl.indexOf("'")==-1) {
				cl="`"+cl+"`";
			}
		//只有一个空格
		}else if(cl.indexOf(" ")!=-1 && cl.indexOf(" ")==cl.lastIndexOf(" ")) {
			//DISTINCT打头
			if(cl.startsWith("DISTINCT") || cl.startsWith("distinct")) {
				cl="DISTINCT `"+cl.substring(cl.indexOf(" ")+1)+"`";
			}else if(cl.indexOf("`")==-1){
				cl="`"+cl.substring(0,cl.indexOf(" "))+"`"+cl.substring(cl.indexOf(" "));
			}		
		}
		return cl;
	}
	
	public static void main(String[] args) {
		//String str="(case a.is_top_showing when 1 then '<span class=\"label-success label\">是</span>' end) as 首页展示";
		//String str="CONCAT(a.id,',',a.name,',',a.original,',',a.thumbnail) as 图片预览";
		//String str="abc,id desc,a.s asc";
		String str="date_format(sn.createDate, '%Y-%m-%d') as 入库时间";
		SqlBuild sBuild=new SqlBuild();
		String c = sBuild.changeDian(str);
		System.out.println(c);
	}
}