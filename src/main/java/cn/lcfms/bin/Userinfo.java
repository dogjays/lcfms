package cn.lcfms.bin;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

public class Userinfo {
	
	@SuppressWarnings("unchecked")
	public static <T> T getUserInfo(String key,HttpServletRequest request){
		HashMap<String, Object> map=(HashMap<String, Object>) request.getAttribute("USERINFO");
		if(!map.containsKey(key)) {
			return null;
		}
		return (T)map.get(key);
	}
	@SuppressWarnings("unchecked")
	public static void putUserInfo(String key,Object value,HttpServletRequest request){
		if(null==request.getAttribute("USERINFO")) {
			HashMap<String, Object> map=new HashMap<>();
			request.setAttribute("USERINFO", map);
		}
		HashMap<String, Object> map=(HashMap<String, Object>) request.getAttribute("USERINFO");
		map.put(key, value);			
	}
	
	@SuppressWarnings("unchecked")
	public static void addUserInfo(String key,String value,HttpServletRequest request){
		if(null==request.getAttribute("USERINFO")) {
			HashMap<String, Object> map=new HashMap<>();
			request.setAttribute("USERINFO", map);
		}
		HashMap<String, Object> map=(HashMap<String, Object>) request.getAttribute("USERINFO");
		if(null==map.get(key)) {
			map.put(key, value);
		}else {
			String str=(String) map.get(key);
			map.put(key, str+value);
		}
	}
}
