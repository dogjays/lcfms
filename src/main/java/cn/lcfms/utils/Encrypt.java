/*
 * MD5 加密算法
 */
package cn.lcfms.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.util.Base64Utils;

public class Encrypt {

	// 全局数组
	private final static String[] strDigits = { "0", "1", "2", "3", "4", "5","6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	private final static String DES = "DES";
    private static final String MAC_NAME = "HmacSHA1";
    private static final String ENCODING = "UTF-8";
	private static Encrypt encrypt;
	private Encrypt(){
		
	}
	public static Encrypt getEncrypt(){
		if(encrypt==null){
			encrypt=new Encrypt();
		}
		return encrypt;
	}
    /**
     * md5加密
     * @param strObj
     * @return
     * @throws UnsupportedEncodingException 
     */
	public String md5(String strObj){
		String resultString = null;
		try {
			resultString = new String(strObj);
			MessageDigest md = MessageDigest.getInstance("MD5");
			// md.digest() 该函数返回值为存放哈希值结果的byte数组
			resultString = this.byteToString(md.digest(strObj.getBytes()));
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}
		return resultString;
	}
	/**
     * Description 根据键值进行加密
     * @param data 
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    public String DESencrypt(String data, String key){
    	try {
    		//System.out.println("加密："+data+"=>"+key);
            byte[] bt = encrypt(data.getBytes(), key.getBytes());
            byte[] encode = Base64Utils.encode(bt);
            String strs = new String(encode,"utf8");
        	//System.out.println(strs);
            return strs;
		} catch (Exception e) {
			return new String();
		}  	
    }
 
    /**
     * Description 根据键值进行解密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    public String DESdecrypt(String data, String key){
    	try {
		    //System.out.println("解密："+data+"=>"+key);
	        if (data == null)
	            return null;
	        byte[] buf = Base64Utils.decode(data.getBytes("utf8"));
	        byte[] bt = decrypt(buf,key.getBytes());
	        //System.out.println(new String(bt));
	        return new String(bt);
		} catch (Exception e) {
			return new String();
		}
       
    }
	// 返回形式为数字跟字符串
	private  String byteToArrayString(byte bByte) {
		int iRet = bByte;
		// System.out.println("iRet="+iRet);
		if (iRet < 0) {
			iRet += 256;
		}
		int iD1 = iRet / 16;
		int iD2 = iRet % 16;
		return strDigits[iD1] + strDigits[iD2];
	}

	// 转换字节数组为16进制字串
	private String byteToString(byte[] bByte) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < bByte.length; i++) {
			sBuffer.append(this.byteToArrayString(bByte[i]));
		}
		return sBuffer.toString();
	}
    /**
     * Description 根据键值进行加密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    private byte[] encrypt(byte[] data, byte[] key) throws Exception {
        // 从原始密钥数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成加密操作
        Cipher cipher = Cipher.getInstance(DES);
        // 用密钥初始化Cipher对象
        cipher.init(Cipher.ENCRYPT_MODE, securekey);
        return cipher.doFinal(data);
    }
     
     
    /**
     * Description 根据键值进行解密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    private byte[] decrypt(byte[] data, byte[] key) throws Exception {
        // 从原始密钥数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks); 
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance(DES); 
       // 用密钥初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey); 
        return cipher.doFinal(data);
    }
    
    /**
     * 使用 HMAC-SHA1 签名方法对对encryptText进行签名
     * @param encryptText 被签名的字符串
     * @param encryptKey 密钥
     * @return 返回被加密后的字符串
     * @throws Exception
     */
    public String HmacSHA1Encrypt( String encryptText, String encryptKey ) throws Exception{
        byte[] data = encryptKey.getBytes( ENCODING );
        // 根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
        SecretKey secretKey = new SecretKeySpec( data, MAC_NAME );
            // 生成一个指定 Mac 算法 的 Mac 对象
        Mac mac = Mac.getInstance( MAC_NAME );
        // 用给定密钥初始化 Mac 对象
        mac.init( secretKey );
        byte[] text = encryptText.getBytes( ENCODING );
        // 完成 Mac 操作
        byte[] digest = mac.doFinal( text );
        StringBuilder sBuilder = bytesToHexString( digest );
        return sBuilder.toString();
    }

    /**
     * 转换成Hex
     * 
     * @param bytesArray
     */
    private StringBuilder bytesToHexString( byte[] bytesArray ){
        if ( bytesArray == null ){
            return null;
        }
        StringBuilder sBuilder = new StringBuilder();
        for ( byte b : bytesArray ){
            String hv = String.format("%02x", b);
            sBuilder.append( hv );
        }
        return sBuilder;
    }
}
