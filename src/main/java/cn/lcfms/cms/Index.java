package cn.lcfms.cms;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cn.lcfms.app.admin.bean.ArticleBean;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.CmsDataBase;
import cn.lcfms.utils.Page;
import cn.lcfms.utils.StringUtils;

public class Index extends CmsDataBase{
	

	public Index(HttpServletRequest request, HttpServletResponse response, int id, BaseService service) {
		super(request, response, id, service);
		// TODO Auto-generated constructor stub
	}
	//获取新闻列表
	public void getNewsByid() {
		service.setTable("articles").column("id","title","desc");
		service.where("category_id=#{category_id}");
		service.setData(id);
		int page=StringUtils.StringToInteger(request.getParameter("page"), 1);
		List<HashMap<String, Object>> list = service.selectPage(10, page);
		request.setAttribute("list", list);
		if(list.size()>0) {
			Long count=(Long)list.get(0).get("count");
			Page obj=new Page(count.intValue(), 10, 5, request);
			request.setAttribute("page", obj);
		}	
		HashMap<String, Object> map = service.setTable("categories").column("id","topid","title").where("id="+id).selectMap();
		request.setAttribute("menu_title_1", map);
		int topid=(int) map.get("topid");
		if(topid!=0) {
			HashMap<String, Object> kap = service.column("id","topid","title").where("id="+topid).selectMap();
			request.setAttribute("menu_title_0", kap);
		}
	}
	//获取内容详情
	public void getDetails() {
		ArticleBean bean=service.selectOne(ArticleBean.class, id);
		request.setAttribute("article", bean);
		service.setData(id).sql("(SELECT id,title FROM articles a WHERE id<#{id} ORDER BY id DESC LIMIT 0,1) UNION (SELECT id,title FROM articles b WHERE id>#{id} LIMIT 0,1)");
		List<HashMap<String, Object>> result = service.getResult();
		result.forEach((HashMap<String, Object> map)->{
    		int i=(int) map.get("id");
    		if(i<id) {
    			request.setAttribute("pre", map);
    		}
    		if(i>id) {
    			request.setAttribute("next", map);
    		}
    	});	
		HashMap<String, Object> map = service.setTable("categories").column("id","topid","title").where("id="+bean.getCategory_id()).selectMap();
		request.setAttribute("menu_title_1", map);
		int topid=(int) map.get("topid");
		if(topid!=0) {
			HashMap<String, Object> kap = service.column("id","topid","title").where("id="+topid).selectMap();
			request.setAttribute("menu_title_0", kap);
		}
		
	}
	
	//获取专家详情
	public void getzj() {
		ArticleBean bean=service.selectOne(ArticleBean.class, id);
		request.setAttribute("article", bean);
		service.leftJoin("files").on("files.file_type_id=articles.id and file_type='articles'");
		service.where("articles.category_id=17");
		service.column("articles.title","articles.id","articles.desc","files.thumbnail","files.original");
		service.groupBy("articles.id");
		List<HashMap<String, Object>> list = service.selectList();	
		for (int i = 0; i < list.size(); i++) {
			HashMap<String, Object> map=list.get(i);
			int id=(int) map.get("id");
			if(id==bean.getId()) {
				String original=(String) map.get("original");
				request.setAttribute("original", original);
				list.remove(map);
				break;
			}
		}
		request.setAttribute("zjlist", list);
		HashMap<String, Object> map = service.setTable("categories").column("id","topid","title").where("id="+bean.getCategory_id()).selectMap();
		request.setAttribute("menu_title_1", map);		
	}
	
	//康复案例视频页面
	public void kangfu() {
		service.setTable("articles").column("id","title","labels");
		service.where("category_id=19");
		service.where("mid=27");
		service.setData(id);
		List<HashMap<String, Object>> list = service.selectList();	
		request.setAttribute("list", list);	
	}
	
}
