package cn.lcfms.test;


import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.lcfms.app.weixin.service.WxApiService;
import cn.lcfms.app.weixin.support.AesException;
import cn.lcfms.app.weixin.support.WXBizMsgCrypt;
import cn.lcfms.app.weixin.util.WeixinDecrypMsg;
import cn.lcfms.utils.Vardump;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:springmvc-context.xml" })
public class WeixinTest {
	@Autowired
	private WxApiService service;
	private static String token="5zbOreI5ypttl1fwlJwYGWeRKF1KXXBI";
	private static String EncodingAESKey="I7uaBYQnDuWk5kv9gk869Zrv1oviP7VR5bi8cBxAnAH";
	private static String appid="wx4bd1b06b61e0f5ef";	

	@Test
	public void t4() {
		//String menu = service.getMenu();
		//System.out.println(menu);
		//service.getMaterialCount("all");
		List<HashMap<String, Object>> list = service.getMaterialNewsList(0, 5);
		Vardump.print(list);
	}
	
	public void t3() {
		// service.getAccess_token();
		// service.getMenu();
		// service.creatMenu("{\"button\":[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"}}");
		JSONObject msg = WeixinDecrypMsg.decryptMsg(service,"4a978c32cc059381d49c88e801a8e42e5c74f0b5", "1528021124", "680035288", "<xml>" + 
				"    <ToUserName><![CDATA[gh_20c2e55c5054]]></ToUserName>" + 
				"    <Encrypt><![CDATA[6yAM98U5RjZyKYzoJ0qxD26QqHHcau5yIE/yjZNPQQN7W6b2N2No9shHc0acRBabovSSK1oK5FreIxTCMznuqT8auOOrvQZ5rnifVzOL52jbjKk8ZEpf/IM3PKB/7V0ZEmo875Mh+UrbfgmPlttyyG4jMvkPAn0LNYGSzynMe316DtW5YziG/jeQVn+hjiEQ+uSWS8zDGbFVMWehPZrCCnAZSWBlIbNil6BQ19ARO83D590WBKZpMI68LP6xHJ4Rl6DQ52bbP/yYrMTARv9khGPgToJPQXGRvA2i6Nbb9ZqgB1jn7A/2r6Z03tNyuC/UpeAowu6tYREbIW0MuO5mVuywOi01rBv++CvmZTKSlbujY92y4K5jAN3elc7oGFN+WTGLnPRC+L8CXqtomy8YVBowsi9X0aiuzYKL9cnqR3ZLXCwk31Q4pRRWHR9K5fMijgYB8Xev2hEZ3YVBdTk2/g==]]></Encrypt>" + 
				"</xml>");
		System.out.println(msg);
		
	}

	
	public void t2() {
		try {
			WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(token,EncodingAESKey,appid);
			String verifyMsgSig = "9b05da23ba229333904a49b1a405617cfa5d6ebd";
			String timeStamp = "1528007022";
			String nonce = "940415947";
			String echoStr = "13544327022468377087";
			echoStr=wxcpt.encrypt(wxcpt.getRandomStr(),echoStr);
			wxcpt.verifyUrl(verifyMsgSig, timeStamp, nonce, echoStr);
		} catch (AesException e) {
			e.printStackTrace();
		}
	}
	
	public void t1() {
		String string="<xml>" +
				"<ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>" + 
				"<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>" + 
				"<CreateTime>1408090651</CreateTime>" + 
				"<MsgType><![CDATA[event]]></MsgType>" + 
				"<Event><![CDATA[pic_sysphoto]]></Event>" + 
				"<EventKey><![CDATA[6]]></EventKey>" + 
				"<SendPicsInfo><Count>1</Count>" + 
				"<PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum>" + 
				"</item>" + 
				"</PicList>" + 
				"</SendPicsInfo>" + 
				"</xml>";
		XMLSerializer xmlSerializer = new XMLSerializer();
	    JSONObject read = (JSONObject) xmlSerializer.read(string);
		System.out.println(read);
	}
}
